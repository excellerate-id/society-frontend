// @flow
import { AppRegistry } from 'react-native';

import App from './src/App';

AppRegistry.registerComponent('Society51', () => App);
