// @flow
import React, { Component } from 'react';
import { TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import colors from '../base-ui/theme/colors';

type State = {
  password: string,
  showPassword: boolean,
}

export default class EnterPasswordPage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      password: '',
      showPassword: false,
    };
  }

  handleConfirm() {
    const password = this.state.password;
    const passwordValidation = /^(?=.*[a-z])(?=.*\d)([a-zA-Z\d]){8,}/;
    const easyPassword = '123abcde' || 'abcde123' || '123qwerty' || 'qwerty123' || '123password' || 'password123';

    if (!passwordValidation.test(password)) {
      Toast.show('Password must contain 8 digits, alphabet and number');
    } else if (password.match(easyPassword)) {
      Toast.show('Password must not contain common words');
    } else {
      Actions.confirmPasswordPage({ password });
    }
  }

  render() {
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Enter password</StyledText>
          </Section>
          <Section style={{ paddingTop: 4 }}>
            <StyledText font="OpenSans-SemiBold-16-24">Minimum 8 digit, alphabet and number.</StyledText>
          </Section>
          <Section style={{ paddingTop: 16, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: colors.dustyGray }}>
            <Col style={{ flex: 1 }}>
              <TextInput
                autoFocus
                secureTextEntry={!this.state.showPassword}
                selectionColor={colors.white}
                selectTextOnFocus
                underlineColorAndroid="transparent"
                style={{ height: 40, color: colors.white }}
                value={this.state.password}
                onChangeText={password => this.setState({ password })}
              />
            </Col>
            <Col style={{ justifyContent: 'center' }}>
              <StyledText
                font="OpenSans-SemiBold-12"
                onPress={() => this.setState({ showPassword: !this.state.showPassword })}
              >
                SHOW
              </StyledText>
            </Col>
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton
              label="Confirm"
              onPress={() => this.handleConfirm()}
            />
          </Section>
        </Body>
      </Page>
    );
  }
}
