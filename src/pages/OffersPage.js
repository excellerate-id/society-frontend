// @flow
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react';

import Body from '../base-ui/Body';
import OfferCard from '../base-ui/OfferCard';
import Page from '../base-ui/Page';
import Section from '../base-ui/Section';
import Separator from '../base-ui/Separator';
import StyledText from '../base-ui/StyledText';

import dashboard from '../api/dashboard';
import dashboardStore from '../stores/dashboardStore';

import greetingTime from '../utils/greetingTime';

@observer
export default class OffersPage extends Component {
  componentDidMount() {
    OffersPage.updateDashboard();
  }

  static async updateDashboard() {
    const result = await dashboard();
    Object.keys(result.data).forEach((key) => {
      dashboardStore[key] = result.data[key];
    });
  }

  render() {
    const { accountDetails, productList } = dashboardStore;
    return (
      <Page>
        <Body contentContainerStyle={{ padding: 16 }}>
          <Section>
            <StyledText font="OpenSans-SemiBold-16-24">{greetingTime()}</StyledText>
            <StyledText font="Domine-Regular-28-40">{accountDetails.name}</StyledText>
          </Section>
          <Section style={{ paddingVertical: 20 }}>
            <Separator />
          </Section>
          <StyledText font="OpenSans-SemiBold-18-26">Here are your offers</StyledText>
          {
            productList.toJS().map(product => (
              <Section key={product.id} style={{ paddingTop: 16 }}>
                <TouchableOpacity onPress={() => Actions.productPage({ product })}>
                  <OfferCard product={product} />
                </TouchableOpacity>
              </Section>
            ))
          }

        </Body>
      </Page>
    );
  }
}
