// @flow
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';

import Body from '../base-ui/Body';
import DatePicker from '../base-ui/DatePicker';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Row from '../base-ui/Row';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import createCallSchedule from '../api/createCallSchedule';

import dashboardStore from '../stores/dashboardStore';

const tomorrow = moment().add(1, 'day').startOf('day').add(10, 'hours');

type State = {
  loading: boolean,
  showDatePicker: boolean,
  showTimePicker: boolean,
  scheduledCallDate: string,
  scheduledCallTime: string,
  markedDates: {
    [key: string]: {
      selected?: boolean,
      marked?: boolean,
    },
  },
};

export default class ScheduleCallPage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
      showDatePicker: false,
      showTimePicker: false,
      scheduledCallDate: tomorrow.format('DD MMMM YYYY'),
      scheduledCallTime: tomorrow.format('kk:mm'),
      markedDates: {
        [tomorrow.format('YYYY-MM-DD')]: { selected: true },
      },
    };
  }

  handleDateSelect(date: string) {
    const scheduledCallDate = moment(date).format('DD MMMM YYYY');
    const markedDates = {};
    markedDates[moment(date).format('YYYY-MM-DD')] = { selected: true };
    this.setState({ showDatePicker: false, scheduledCallDate, markedDates });
  }

  handleTimeConfirm(time: string) {
    const scheduledCallTime = moment(time).format('kk:mm');
    this.setState({ showTimePicker: false, scheduledCallTime });
  }

  handleTimeCancel() {
    this.setState({ showTimePicker: false });
  }

  async handleSubmit() {
    this.setState({ loading: true });
    try {
      const { scheduledCallDate, scheduledCallTime } = this.state;
      await createCallSchedule({
        userId: dashboardStore.accountDetails.id,
        scheduledCallDate,
        scheduledCallTime,
      });
      this.setState({ loading: false });
      Actions.successPage({
        title: 'Thank you!',
        description: 'Our Relationship Manager has been notified and will be in touch with you soon.',
        label: 'OK',
        onConfirm: () => Actions.popTo('productPage'),
      });
    } catch (error) {
      this.setState({ loading: false });
      Toast.show(error.message);
    }
  }

  render() {
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 20 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Schedule your call</StyledText>
          </Section>
          <Section style={{ paddingTop: 4 }}>
            <StyledText font="OpenSans-SemiBold-16-24">
              Any preferred call time? Let us know when works for you.
            </StyledText>
          </Section>
          <Section style={{ paddingTop: 22 }}>
            <StyledText font="OpenSans-Bold-11">DATE</StyledText>
          </Section>
          <Section style={{ marginTop: 10, height: 45, backgroundColor: 'transparent', justifyContent: 'center', borderBottomWidth: 1, borderColor: '#999999' }}>
            <TouchableOpacity onPress={() => this.setState({ showDatePicker: true })}>
              <Row style={{ justifyContent: 'space-between' }}>
                <StyledText font="OpenSans-SemiBold-16-24">
                  {this.state.scheduledCallDate}
                </StyledText>
                <Icon name="calendar-blank" color="white" size={20} />
              </Row>
            </TouchableOpacity>
          </Section>
          <Section style={{ paddingTop: 15 }}>
            <StyledText font="OpenSans-Bold-11">TIME</StyledText>
          </Section>
          <Section style={{ marginTop: 10, height: 45, backgroundColor: 'transparent', justifyContent: 'center', borderBottomWidth: 1, borderColor: '#999999' }}>
            <TouchableOpacity onPress={() => this.setState({ showTimePicker: true })}>
              <Row style={{ justifyContent: 'space-between' }}>
                <StyledText font="OpenSans-SemiBold-16-24">
                  {this.state.scheduledCallTime}
                </StyledText>
                <Icon name="clock" color="white" size={20} />
              </Row>
            </TouchableOpacity>
          </Section>
          <Section style={{ paddingTop: 30 }}>
            <PrimaryButton
              label="Call me at this time"
              onPress={() => this.handleSubmit()}
            />
          </Section>
        </Body>
        <DatePicker
          visible={this.state.showDatePicker}
          onRequestClose={() => this.setState({ showDatePicker: false })}
          current={moment(this.state.scheduledCallDate, 'DD MMMM YYYY').format('YYYY-MM-DD')}
          markedDates={this.state.markedDates}
          minDate={moment().add(1, 'day').format('YYYY-MM-DD')}
          onDayPress={day => this.handleDateSelect(new Date(day.timestamp))}
        />
        <DateTimePicker
          date={tomorrow.toDate()}
          isVisible={this.state.showTimePicker}
          mode="time"
          onConfirm={time => this.handleTimeConfirm(time)}
          onCancel={() => this.handleTimeCancel()}
        />
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
