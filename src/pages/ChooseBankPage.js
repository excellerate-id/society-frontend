// @flow
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Header from '../base-ui/Header';
import MenuList from '../base-ui/MenuList';
import Page from '../base-ui/Page';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';

import checkImage from '../images/ic_check.png';

type Bank = {
  id: number,
  name: string,
  image: string,
  code: string,
  instructions: Array<string>,
};

type Props = {
  banks: Array<Bank>,
};

type State = {
  loading: boolean,
};

export default class ChooseBankPage extends Component {
  props: Props;
  state: State;

  state = {
    loading: false,
  };

  render() {
    const { banks } = this.props;
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Choose Payment Method</StyledText>
          </Section>
          <Section style={{ paddingTop: 22 }}>
            <MenuList
              items={banks.map(bank => ({
                id: bank.id,
                imageSource: { uri: bank.image },
                label: bank.name,
                onPress: () => Actions.virtualAccountDetailsPage({ bank }),
              }))}
            />
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <MenuList
              items={[{
                id: 1,
                imageSource: checkImage,
                label: 'Write a Check',
                onPress: () => Actions.writeCheckPage(),
              }]}
            />
          </Section>
        </Body>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
