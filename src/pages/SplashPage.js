// @flow
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Toast from 'react-native-simple-toast';
import { action } from 'mobx';
import { observer } from 'mobx-react/native';

import Page from '../base-ui/Page';
import PageBackground from '../base-ui/PageBackground';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

import dashboard from '../api/dashboard';
import registerDevice from '../api/registerDevice';
import authStore from '../stores/authStore';
import dashboardStore from '../stores/dashboardStore';

@observer
export default class SplashPage extends Component {
  componentDidMount() {
    SplashPage.initApp();
  }

  @action
  static async initApp() {
    try {
      await registerDevice();
      authStore.sessionToken = await AsyncStorage.getItem('@sessionToken');
      if (authStore.sessionToken == null) {
        Actions.reset('enterAccessCodePage');
      } else {
        const result = await dashboard();
        Object.keys(result.data).forEach((key) => {
          dashboardStore[key] = result.data[key];
        });
        Actions.reset('mainTabs');
      }
    } catch (error) {
      if (error.statusCode === 401) {
        Actions.reset('loginPage');
      }
      Toast.show(error.message);
    }
  }

  render() {
    return (
      <Page>
        <PageBackground style={{ justifyContent: 'center' }}>
          <Section style={{ alignItems: 'center' }}>
            <StyledText font="OpenSans-SemiBold-18">Welcome to</StyledText>
          </Section>
          <Section style={{ paddingTop: 9, alignItems: 'center' }}>
            <StyledText font="Didot-Bold-36">SOCIETY 51</StyledText>
          </Section>
          <Section style={{ paddingTop: 7, alignItems: 'center' }}>
            <StyledText font="OpenSans-SemiBold-18-26">www.society51.com</StyledText>
          </Section>
          <Section style={{ paddingTop: 35, alignItems: 'center', paddingHorizontal: 45 }}>
            <StyledText font="OpenSans-SemiBoldItalic-18-26" style={{ textAlign: 'center' }}>
              The invite-only club for exclusive investment opportunities.
            </StyledText>
          </Section>
        </PageBackground>
      </Page>
    );
  }
}
