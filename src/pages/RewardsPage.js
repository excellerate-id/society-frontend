// @flow
import React, { Component } from 'react';
import { observer } from 'mobx-react';

import Body from '../base-ui/Body';
import Page from '../base-ui/Page';
import RewardCard from '../base-ui/RewardCard';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

import dashboardStore from '../stores/dashboardStore';

@observer
export default class RewardsPage extends Component {
  render() {
    const { accountDetails } = dashboardStore;
    return (
      <Page>
        <Body contentContainerStyle={{ flex: 1, padding: 16, alignItems: 'center' }}>
          <Section style={{ alignSelf: 'flex-start' }}>
            <StyledText font="Domine-Regular-28-40">Rewards</StyledText>
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <RewardCard account={accountDetails} />
          </Section>
          <Section style={{ flex: 1, justifyContent: 'center' }}>
            <StyledText font="OpenSans-Italic-16-20" style={{ textAlign: 'center' }}>{[
              'Our Reward Program will be starting soon. ',
              'Please stay tuned.'].join('')}
            </StyledText>
          </Section>
        </Body>
      </Page>
    );
  }
}
