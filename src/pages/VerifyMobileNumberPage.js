// @flow
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Actions } from 'react-native-router-flux';
import SmsListener from 'react-native-android-sms-listener';

import Body from '../base-ui/Body';
import DigitInput from '../base-ui/DigitInput';
import Header from '../base-ui/Header';
import Link from '../base-ui/Link';
import Page from '../base-ui/Page';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import resendVerificationCode from '../api/resendVerificationCode';
import validateMobileNumber from '../api/validateMobileNumber';

import authStore from '../stores/authStore';
import userStore from '../stores/userStore';

import formatCountDown from '../utils/formatCountDown';

const WAIT_DURATION = 30;

type Props = {
  accessCode: '',
};

type State = {
  digits: Array<string>,
  count: number,
  resendSmsLinkDisabled: boolean,
};

export default class VerifyMobileNumberPage extends Component {
  props: Props;
  state: State;
  timer: any;
  smsListener: any;

  constructor() {
    super();
    this.state = {
      digits: ['', '', '', ''],
      count: WAIT_DURATION,
      resendSmsLinkDisabled: true,
    };
  }

  componentDidMount() {
    this.startTimer();
    this.smsListener = SmsListener.addListener((message) => {
      const code = message.body.replace('Your Society51 validation code ', '');
      if (code.length === 4) {
        const digits = message.body.split('');
        this.setState({ digits });
        this.handleChangeDigits(digits);
      }
    });
  }

  componentWillUnmount() {
    clearInterval(this.timer);
    if (this.smsListener) {
      this.smsListener.remove();
    }
  }

  startTimer() {
    this.setState({ resendSmsLinkDisabled: true, count: WAIT_DURATION });
    clearInterval(this.timer);
    this.timer = setInterval(() => {
      const nextCount = this.state.count - 1;
      this.setState({ count: nextCount });
      if (nextCount === 0) {
        clearInterval(this.timer);
        this.setState({ resendSmsLinkDisabled: false });
      }
    }, 1000);
  }

  async handleChangeDigits(digits: Array<string>) {
    this.setState({ digits });
    const verificationCode = digits.join('');
    if (verificationCode.length === 4) {
      try {
        const result = await validateMobileNumber(verificationCode);
        await AsyncStorage.setItem('@sessionToken', result.success.token);
        authStore.sessionToken = result.success.token;
        Actions.reset('verifyDetailsPage');
      } catch (error) {
        Toast.show(error.message);
      }
    }
  }

  async handleResend() {
    this.startTimer();
    try {
      await resendVerificationCode(this.props.accessCode);
    } catch (error) {
      Toast.show(error.message);
    }
  }

  render() {
    return (
      <Page>
        <Header />
        <Body>
          <Section style={{ paddingHorizontal: 20 }}>
            <StyledText font="Domine-Regular-28-40">Verify mobile number</StyledText>
          </Section>
          <Section style={{ paddingTop: 4, paddingHorizontal: 20 }}>
            <StyledText font="OpenSans-SemiBold-16-24">
              Please enter 4-digit pin code that we&apos;ve
              sent to {userStore.mobileNumber}.
            </StyledText>
          </Section>
          <Section style={{ paddingTop: 16, alignItems: 'center' }}>
            <DigitInput
              digits={this.state.digits}
              onChangeDigits={digits => this.handleChangeDigits(digits)}
            />
          </Section>
          <Section style={{ paddingTop: 16, alignItems: 'center' }}>
            <StyledText font="OpenSans-Bold-14-20">{formatCountDown(this.state.count)}</StyledText>
          </Section>
          <Section style={{ paddingTop: 16, alignItems: 'center' }}>
            <Link
              disabled={this.state.resendSmsLinkDisabled}
              label="No SMS? Resend"
              onPress={() => this.handleResend()}
            />
          </Section>
        </Body>
      </Page>
    );
  }
}
