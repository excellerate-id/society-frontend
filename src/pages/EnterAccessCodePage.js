// @flow
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react/native';

import AccessCodeInput from '../base-ui/AccessCodeInput';
import Body from '../base-ui/Body';
import Link from '../base-ui/Link';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import validateAccessCode from '../api/validateAccessCode';
import userStore from '../stores/userStore';

type State = {
  loading: boolean,
  accessCode: string,
  submitButtonDisabled: boolean,
};

@observer
export default class EnterAccessCodePage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
      accessCode: '',
      submitButtonDisabled: true,
    };
  }

  handleChangeText(accessCode: string) {
    this.setState({ accessCode });
    if (accessCode.length >= 6) {
      this.setState({ submitButtonDisabled: false });
    } else {
      this.setState({ submitButtonDisabled: true });
    }
  }

  async handleSubmit() {
    this.setState({ loading: true });
    try {
      const result = await validateAccessCode(this.state.accessCode);
      Object.keys(result).forEach((key) => {
        userStore[key] = result[key];
      });
      this.setState({ loading: false });
      Actions.verifyMobileNumberPage({ accessCode: this.state.accessCode });
    } catch (error) {
      this.setState({ loading: false });
      setTimeout(() => Toast.show(error.message), 500);
    }
  }

  render() {
    return (
      <Page>
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section style={{ paddingTop: 36 }}>
            <StyledText font="Domine-Regular-28-40">
              Enter your access code
            </StyledText>
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <AccessCodeInput
              value={this.state.accessCode}
              onChangeText={accessCode => this.handleChangeText(accessCode)}
            />
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton
              disabled={this.state.submitButtonDisabled}
              label="Submit"
              onPress={() => this.handleSubmit()}
            />
          </Section>
          <Section style={{ paddingTop: 40 }}>
            <StyledText font="OpenSans-SemiBold-16-24">
              If you do not have one yet, please
              contact an existing Society51 member to
              get your access code.
            </StyledText>
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <Link
              label="Already registered? Login."
              onPress={() => Actions.reset('loginPage')}
            />
          </Section>
        </Body>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
