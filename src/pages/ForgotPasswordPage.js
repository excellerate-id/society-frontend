// @flow
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';
import TextInputField from '../base-ui/TextInputField';
import Toast from '../base-ui/Toast';

import resetPassword from '../api/resetPassword';

export default class ForgotPasswordPage extends Component {
  state = {
    email: '',
  };

  async handleResetPassword() {
    try {
      const result = await resetPassword(this.state.email);
      Actions.successPage({
        title: 'E-mail Sent',
        description: 'Please check your e-mail for next instruction on resetting your password.',
        label: 'Start Investing!',
        onConfirm: () => Actions.popTo('loginPage'),
      });
    } catch (error) {
      Toast.show(error.message);
    }
  }

  render() {
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 20 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Forgot Password</StyledText>
            <StyledText font="OpenSans-SemiBold-16-24">Write down your registered e-mail</StyledText>
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <TextInputField
              autoCapitalize="none"
              keyboardType="email-address"
              label="E-MAIL"
              placeholder="E-mail"
              value={this.state.email}
              onChangeText={email => this.setState({ email })}
            />
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton
              label="Reset Password"
              onPress={() => this.handleResetPassword()}
            />
          </Section>
        </Body>
      </Page>
    );
  }
}
