// @flow
import React, { Component } from 'react';

import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';
import TransactionHistoryCard from '../base-ui/TransactionHistoryCard';

import dashboardStore from '../stores/dashboardStore';

import formatTransactionDate from '../utils/formatTransactionDate';
import formatPrice from '../utils/formatPrice';

export default class TransactionsHistoryPage extends Component {
  state = {
    type: 'Invest',
  }

  render() {
    const transactions = dashboardStore.transactionHistory.toJS();
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 16, paddingBottom: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Transaction History</StyledText>
          </Section>
          {transactions.map(transaction => (
            <Section key={transaction.id} style={{ paddingTop: 22 }}>
              <StyledText font="OpenSans-SemiBold-14" style={{ fontSize: 14 }}>
                {formatTransactionDate(transaction.created)}
              </StyledText>
              <Col style={{ paddingTop: 8 }}>
                <TransactionHistoryCard
                  amount={formatPrice('', transaction.amountinvested)}
                  title={transaction.title}
                  type={this.state.type}
                  created={transaction.created}
                />
              </Col>
            </Section>
          ))}
        </Body>
      </Page>
    );
  }
}
