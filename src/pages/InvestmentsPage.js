// @flow
import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react';

import Body from '../base-ui/Body';
import InvestmentCard from '../base-ui/InvestmentCard';
import Page from '../base-ui/Page';
import Row from '../base-ui/Row';
import SecondaryButton from '../base-ui/SecondaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

import dashboardStore from '../stores/dashboardStore';
import formatPrice from '../utils/formatPrice';

@observer
export default class InvestmentsPage extends Component {
  static renderInvestmentCards() {
    const products = dashboardStore.productList;
    const investments = dashboardStore.yourInvestment;
    const cards = [];
    investments.forEach((investment) => {
      const investedProduct = products.find(product => product.id === investment.productid);
      if (investedProduct) {
        cards.push(
          <Section key={investment.productid} style={{ paddingVertical: 10 }}>
            <TouchableOpacity
              onPress={() => Actions.productPage({
                invested: true,
                amount: Number(investment.investment),
                product: investedProduct,
              })}
            >
              <InvestmentCard
                image={investedProduct.image}
                title={investment.title}
                rate={investedProduct.interestRate}
                amount={formatPrice(investedProduct.currency, investment.investment)}
                nextRepayment={investedProduct.repaymentDate}
              />
            </TouchableOpacity>
          </Section>,
        );
      }
    });
    return cards;
  }

  render() {
    const investments = dashboardStore.yourInvestment;
    let totalInvestment = 0;
    investments.forEach((investment) => { totalInvestment += Number(investment.investment); });
    return (
      <Page>
        <Body contentContainerStyle={{ padding: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Your investments</StyledText>
          </Section>
          <Section style={{ paddingTop: 19 }}>
            <Row style={{ justifyContent: 'space-between' }}>
              <StyledText font="OpenSans-SemiBold-16-24">Total Investment</StyledText>
              <StyledText font="OpenSans-Regular-16-24">{formatPrice('IDR', totalInvestment)}</StyledText>
            </Row>
          </Section>
          <Section style={{ paddingBottom: 20, paddingTop: 16 }}>
            <SecondaryButton
              inline
              label="See Transaction History"
              small
              onPress={() => Actions.transactionsHistoryPage()}
            />
          </Section>
          {InvestmentsPage.renderInvestmentCards()}
        </Body>
      </Page>
    );
  }
}
