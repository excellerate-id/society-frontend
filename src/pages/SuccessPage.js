// @flow
import React, { Component } from 'react';

import Body from '../base-ui/Body';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

type Props = {
  title: string,
  description: string,
  label: string,
  onConfirm: () => void,
};

export default class SuccessPage extends Component {
  props: Props;

  render() {
    const { title, description, label, onConfirm } = this.props;

    return (
      <Page>
        <Body contentContainerStyle={{ flex: 1, justifyContent: 'center' }}>
          <Section style={{ alignItems: 'center' }}>
            <StyledText font="Domine-Regular-28-40">{title}</StyledText>
          </Section>
          <Section style={{ paddingTop: 35, alignItems: 'center', paddingHorizontal: 35 }}>
            <StyledText font="OpenSans-SemiBold-14-20" style={{ textAlign: 'center' }}>{description}</StyledText>
          </Section>
          <Section style={{ paddingTop: 25, paddingHorizontal: 35 }}>
            <PrimaryButton label={label} onPress={onConfirm} />
          </Section>
        </Body>
      </Page>
    );
  }
}
