// @flow
import React, { Component } from 'react';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react';

import Header from '../base-ui/Header';
import Link from '../base-ui/Link';
import MultilineTextInputField from '../base-ui/MultilineTextInputField';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import TextInputField from '../base-ui/TextInputField';
import Toast from '../base-ui/Toast';

import updateProfile from '../api/updateProfile';
import getSalesContacts from '../api/getSalesContacts';
import dashboardStore from '../stores/dashboardStore';

type State = {
  loading: boolean,
}

@observer
export default class EditProfilePage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  async handleSubmit() {
    this.setState({ loading: true });
    const { id, name, email, address, city } = dashboardStore.accountDetails;
    try {
      await updateProfile({
        id,
        name,
        email,
        address,
        city,
      });
      this.setState({ loading: false });
      Actions.pop();
    } catch (error) {
      this.setState({ loading: false });
      Toast.show(error.message);
    }
  }

  async handleCallUs() {
    this.setState({ loading: true });
    try {
      const contacts = await getSalesContacts();
      this.setState({ loading: false });
      Actions.callUsPage({ contacts });
    } catch (error) {
      this.setState({ loading: false });
      Toast.show(error.message);
    }
  }

  render() {
    const { accountDetails } = dashboardStore;
    return (
      <Page>
        <Header title="Edit Profile" />
        <KeyboardAwareScrollView contentContainerStyle={{ paddingHorizontal: 20 }}>
          <Section style={{ paddingTop: 16 }}>
            <TextInputField
              autoCapitalize="none"
              keyboardType="email-address"
              label="E-MAIL"
              placeholder="E-mail"
              value={accountDetails.email}
              onChangeText={(email) => { accountDetails.email = email; }}
            />
            <MultilineTextInputField
              label="ADDRESS"
              value={accountDetails.address}
              onChangeText={(address) => { accountDetails.address = address; }}
            />
            <TextInputField
              autoCapitalize="none"
              keyboardType="default"
              label="CITY"
              placeholder="Jakarta"
              value={accountDetails.city}
              onChangeText={(city) => { accountDetails.city = city; }}
            />
          </Section>
          <Section style={{ paddingVertical: 20 }}>
            <PrimaryButton label="Submit" onPress={() => this.handleSubmit()} />
          </Section>
          <Section style={{ paddingTop: 16, alignItems: 'center' }}>
            <Link
              label="Want to change something else? Call us."
              onPress={() => this.handleCallUs()}
            />
          </Section>
        </KeyboardAwareScrollView>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
