// @flow
import React, { Component } from 'react';
import { AsyncStorage, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import colors from '../base-ui/theme/colors';

import activateUser from '../api/activateUser';

type Props = {
  password: string,
}

type State = {
  loading: boolean,
  passwordConfirm: '',
  showPassword: boolean,
}

export default class ConfirmPasswordPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
      passwordConfirm: '',
      showPassword: false,
    };
  }

  async handleCreateMyAccount() {
    this.setState({ loading: true });
    if (this.state.passwordConfirm.length < 8) {
      this.setState({ loading: false });
      Toast.show('Password should be at least 8 characters');
    } else if (this.props.password !== this.state.passwordConfirm) {
      this.setState({ loading: false });
      Toast.show('Password does not match');
    } else {
      try {
        const result = await activateUser(this.state.passwordConfirm);
        await AsyncStorage.setItem('@sessionToken', result.success.token);
        this.setState({ loading: false });
        Actions.reset('successPage', {
          title: 'Congratulations!',
          description: [
            'You have successfully activated your access to Society 51 account. ',
            'You now have access to exclusive investment opportunities right in the ',
            'palm of your hand.'].join(''),
          label: 'Start Investing!',
          onConfirm: () => Actions.reset('mainTabs'),
        });
      } catch (error) {
        this.setState({ loading: false });
        Toast.show(error.message);
      }
    }
  }

  render() {
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Confirm password</StyledText>
          </Section>
          <Section style={{ paddingTop: 4 }}>
            <StyledText font="OpenSans-SemiBold-16-24">Minimum 8 digit, alphabet and number.</StyledText>
          </Section>
          <Section style={{ paddingTop: 16, flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: colors.dustyGray }}>
            <Col style={{ flex: 1 }}>
              <TextInput
                autoFocus
                secureTextEntry={!this.state.showPassword}
                selectionColor={colors.white}
                selectTextOnFocus
                underlineColorAndroid="transparent"
                style={{
                  height: 40,
                  color: colors.white,
                }}
                value={this.state.passwordConfirm}
                onChangeText={passwordConfirm => this.setState({ passwordConfirm })}
              />
            </Col>
            <Col style={{ justifyContent: 'center' }}>
              <StyledText
                font="OpenSans-SemiBold-12"
                onPress={() => this.setState({ showPassword: !this.state.showPassword })}
              >
                SHOW
              </StyledText>
            </Col>
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton
              label="Create My Account"
              onPress={() => this.handleCreateMyAccount()}
            />
          </Section>
        </Body>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
