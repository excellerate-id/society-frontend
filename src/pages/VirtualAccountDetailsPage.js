// @flow
import React, { Component } from 'react';
import { Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Accordion from '../base-ui/Accordion';
import Body from '../base-ui/Body';
import BulletNumber from '../base-ui/BulletNumber';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import SecondaryButton from '../base-ui/SecondaryButton';
import Section from '../base-ui/Section';
import Separator from '../base-ui/Separator';
import StyledText from '../base-ui/StyledText';
import TextBoxField from '../base-ui/TextBoxField';

type Instruction = {
  id: number,
  bankId: number,
  title: string,
  paymentInstructionDetail: Array<{
    id: number,
    instructionId: number,
    stepNo: number,
    description: string,
  }>
};

type Bank = {
  name: string,
  image: string,
  code: string,
  id: number,
  virtualAccount: {
    productId: number,
    accountNumber: string,
    accountName: string,
    userId: number,
    amountInvested: number,
  },
  instructions: Array<Instruction>,
};

type Props = {
  bank: Bank,
};

export default class VirtualAccountDetailsPage extends Component {
  props: Props;

  render() {
    const { bank } = this.props;
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section style={{ paddingBottom: 16 }}>
            <Col style={{ borderRadius: 2, backgroundColor: '#FFF', height: 38, width: 80, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={{ uri: bank.image }}
                resizeMode="contain"
                style={{ height: 30, width: 72, backgroundColor: 'transparent' }}
              />
            </Col>
          </Section>
          <Section style={{ paddingBottom: 10 }}>
            <StyledText font="Domine-Regular-28-40">
              Transfer your investment to virtual account number below
            </StyledText>
            <StyledText font="OpenSans-SemiBold-14">Minimum Rp 100 mio</StyledText>
          </Section>
          <Section style={{ paddingBottom: 16 }}>
            <TextBoxField
              clipboard
              label="Virtual Account Number"
              value={bank.virtualAccount.accountNumber}
            />
            <TextBoxField
              label="Account Name"
              value={bank.virtualAccount.accountName || '-'}
            />
          </Section>
          <Section style={{ paddingBottom: 12 }}>
            <StyledText font="OpenSans-SemiBold-14">How to transfer</StyledText>
          </Section>
          <Separator />
          {bank.instructions.map((instruction, index) => (
            <Section key={instruction.id}>
              <Accordion label={instruction.title} collapsed={index !== 0}>
                <Col style={{ paddingBottom: 10 }}>
                  {instruction.paymentInstructionDetail.map(step => (
                    <Col key={step.id} style={{ flexDirection: 'row', paddingVertical: 5 }}>
                      <BulletNumber label={step.stepNo.toString()} />
                      <Col style={{ width: 10 }} />
                      <StyledText font="OpenSans-Regular-12-20">{step.description}</StyledText>
                    </Col>
                  ))}
                </Col>
              </Accordion>
              <Separator />
            </Section>
          ))}
          <Section style={{ paddingVertical: 20 }}>
            <SecondaryButton label="Return to Offer" onPress={() => Actions.reset('mainTabs')} />
          </Section>
        </Body>
      </Page>
    );
  }
}
