// @flow
import React, { Component } from 'react';
import { Linking } from 'react-native';
import HTMLView from 'react-native-htmlview';
import { Actions } from 'react-native-router-flux';
import Moment from 'moment';

import Avatar from '../base-ui/Avatar';
import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import FooterButton from '../base-ui/FooterButton';
import Header from '../base-ui/Header';
import InvestmentCardHeader from '../base-ui/InvestmentCardHeader';
import Page from '../base-ui/Page';
import Row from '../base-ui/Row';
import SecondaryButton from '../base-ui/SecondaryButton';
import Section from '../base-ui/Section';
import Separator from '../base-ui/Separator';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';
import Toast from '../base-ui/Toast';

import getBankList from '../api/getBankList';
import getSalesContacts from '../api/getSalesContacts';

import formatPrice from '../utils/formatPrice';

type Props = {
  invested: boolean,
  amount: number,
  product: Object,
};

type State = {
  loading: boolean,
};

export default class ProductPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  async handleCallUs() {
    this.setState({ loading: true });
    try {
      const contacts = await getSalesContacts();
      this.setState({ loading: false });
      Actions.callUsPage({ contacts });
    } catch (error) {
      this.setState({ loading: false });
      Toast.show(error.message);
    }
  }

  async handleBankList() {
    this.setState({ loading: true });
    try {
      const banks = await getBankList(this.props.product.id);
      this.setState({ loading: false });
      Actions.chooseBankPage({ banks });
    } catch (error) {
      this.setState({ loading: false });
      Toast.show(error.message);
    }
  }

  static renderKeyValue(key: string, value: string, unit: string = '') {
    if (value == null) {
      return null;
    }
    return (
      <Row style={{ justifyContent: 'space-between', marginVertical: 2 }}>
        <Col style={{ flex: 1 }}>
          <StyledText font="OpenSans-SemiBold-14">{key}</StyledText>
        </Col>
        <Col>
          <StyledText font="OpenSans-Regular-14-20">: </StyledText>
        </Col>
        <Col style={{ flex: 1 }}>
          <StyledText font="OpenSans-Regular-14-20">{value}{unit}</StyledText>
        </Col>
      </Row>
    );
  }

  render() {
    const { invested, amount, product } = this.props;
    return (
      <Page>
        <Header title={product.title} />
        <Body>
          <InvestmentCardHeader invested={invested} amount={amount} product={product} />
          <Section style={{ padding: 20 }}>
            <Col>
              {ProductPage.renderKeyValue('Type', product.type)}
              {ProductPage.renderKeyValue('Start date', product.startAt && Moment(product.startAt).format('DD MMMM YYYY'))}
              {ProductPage.renderKeyValue('Maturity', product.maturityAt && Moment(product.maturityAt).format('DD MMMM YYYY'))}
              {ProductPage.renderKeyValue('Interest rate', product.interestRate, '% p.a.')}
              {ProductPage.renderKeyValue('Tenure', product.tenure, product.tenure > 1 ? ' days' : ' day')}
              {ProductPage.renderKeyValue('Repayment date', product.repaymentDate)}
              {ProductPage.renderKeyValue('Collateral', product.collateral)}
              {ProductPage.renderKeyValue('Total quantum', product.totalQuantum && formatPrice(product.currency, product.totalQuantum))}
            </Col>
            <Col style={{ paddingTop: 20 }}>
              {ProductPage.renderKeyValue('Min. investment', product.minInvestment && formatPrice(product.currency, product.minInvestment))}
              {ProductPage.renderKeyValue('Max. investment', product.maxInvestment && formatPrice(product.currency, product.maxInvestment))}
            </Col>
            <Col style={{ paddingTop: 20 }}>
              { product.description ? (
                <HTMLView
                  value={product.description.replace(new RegExp('<p><br></p>', 'g'), '')}
                  textComponentProps={{
                    style: {
                      color: '#FFF',
                      fontFamily: 'OpenSans-Regular',
                      fontSize: 14,
                      lineHeight: 20,
                    },
                  }}
                />
              ) : null}
            </Col>
            <Col style={{ paddingVertical: 20 }}>
              {product.factsheetUrl != null ? (
                <SecondaryButton
                  inline
                  label="Download Factsheet"
                  small
                  onPress={() => Linking.openURL(product.factsheetUrl)}
                />
              ) : null}
            </Col>
            <Separator />
          </Section>
          <Section style={{ paddingHorizontal: 20, paddingBottom: 30 }}>
            <StyledText font="OpenSans-SemiBold-14-20">Endorsed by</StyledText>
            <Col style={{ paddingTop: 10, justifyContent: 'center' }}>
              {product.endorsedBy.map(endorsedBy => (
                <Row key={endorsedBy.id} style={{ flex: 1, alignItems: 'center', paddingVertical: 8 }}>
                  <Avatar image={endorsedBy.photo} />
                  <Col style={{ paddingHorizontal: 12 }}>
                    <StyledText font="OpenSans-Regular-14-20">{endorsedBy.name}</StyledText>
                  </Col>
                </Row>
              ))}
            </Col>
          </Section>
        </Body>
        {this.props.invested ? null : (
          <FooterButton
            disabled={this.state.loading}
            onCallUs={() => this.handleCallUs()}
            onInvest={() => this.handleBankList()}
          />
        )}
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
