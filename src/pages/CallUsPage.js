// @flow
import React, { Component } from 'react';
import { Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Avatar from '../base-ui/Avatar';
import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import IconButton from '../base-ui/IconButton';
import MenuList from '../base-ui/MenuList';
import Page from '../base-ui/Page';
import Row from '../base-ui/Row';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

import calendarImage from '../images/ic_calendar.png';

type Contact = {
  id: number,
  photo: string,
  name: string,
  title: string,
  phoneNumber: string,
};

type Props = {
  contacts: Array<Contact>,
};

export default class CallUsPage extends Component {
  props: Props;

  render() {
    const { contacts } = this.props;
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ padding: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Call us now</StyledText>
          </Section>
          <Section style={{ paddingTop: 4 }}>
            <StyledText font="OpenSans-SemiBold-14-20">
              We would love to help you on more information about the product
              as well as settling your investment.
            </StyledText>
          </Section>
          <Section style={{ paddingTop: 24 }}>
            {contacts.map(contact => (
              <Row key={contact.id} style={{ paddingTop: 10 }}>
                <Avatar image={contact.photo} />
                <Col style={{ flex: 1, paddingLeft: 12, justifyContent: 'center' }}>
                  <StyledText font="OpenSans-Bold-14-20">{contact.name}</StyledText>
                  <StyledText font="OpenSans-Italic-14-20">{contact.title}</StyledText>
                </Col>
                <IconButton
                  name="phone"
                  onPress={() => Linking.openURL(`tel:${contact.phoneNumber}`)}
                />
              </Row>
            ))}
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <StyledText font="OpenSans-SemiBold-16-24">
              Or we can contact you later
            </StyledText>
          </Section>
          <Section style={{ marginTop: 16 }}>
            <MenuList
              items={[{
                id: 1,
                imageSource: calendarImage,
                label: 'Schedule a Call',
                onPress: () => Actions.scheduleCallPage(),
              }]}
            />
          </Section>
        </Body>
      </Page>
    );
  }
}
