// @flow
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import TextInputField from '../base-ui/TextInputField';
import Toast from '../base-ui/Toast';

import changePassword from '../api/changePassword';
import authStore from '../stores/authStore';

type State = {
  loading: boolean,
  oldPassword: string,
  newPassword: string,
  confirmPassword: string,
}

export default class EnterPasswordPage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
    };
  }

  async handleSubmit() {
    const { oldPassword, newPassword, confirmPassword } = this.state;
    const passwordValidation = /^(?=.*[a-z])(?=.*\d)([a-zA-Z\d]){8,}/;
    const easyPassword = '123abcde' || 'abcde123' || '123qwerty' || 'qwerty123' || '123password' || 'password123';

    if (oldPassword === newPassword) {
      Toast.show('New password cannot be same as the old password');
    } else if (newPassword !== confirmPassword) {
      Toast.show('New password is not same with confirm password');
    } else if (!passwordValidation.test(confirmPassword)) {
      Toast.show('Password must contain 8 digits, alphabet and number');
    } else if (newPassword.match(easyPassword)) {
      Toast.show('Password must not contain common words');
    } else {
      this.setState({ loading: true });
      try {
        await changePassword(this.state.oldPassword, this.state.newPassword);
        authStore.sessionToken = null;
        this.setState({ loading: false });
        // TODO: wait for change password api return new token
        // Actions.pop();
        Actions.reset('loginPage');
      } catch (error) {
        Toast.show(error.message);
        this.setState({ loading: false });
        setTimeout(() => Toast.show(error.message), 500);
      }
    }
  }

  render() {
    return (
      <Page>
        <Header title="Change Password" />
        <Body contentContainerStyle={{ paddingHorizontal: 16 }}>
          <Section>
            <TextInputField
              label="EXISTING PASSWORD"
              secureTextEntry
              value={this.state.oldPassword}
              onChangeText={oldPassword => this.setState({ oldPassword })}
            />
            <TextInputField
              label="NEW PASSWORD"
              secureTextEntry
              value={this.state.newPassword}
              onChangeText={newPassword => this.setState({ newPassword })}
            />
            <TextInputField
              label="CONFIRM PASSWORD"
              secureTextEntry
              value={this.state.confirmPassword}
              onChangeText={confirmPassword => this.setState({ confirmPassword })}
            />
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton
              label="Save"
              onPress={() => this.handleSubmit()}
            />
          </Section>
        </Body>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
