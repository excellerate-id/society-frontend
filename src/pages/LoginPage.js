// @flow
import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import { Actions } from 'react-native-router-flux';

import Body from '../base-ui/Body';
import Link from '../base-ui/Link';
import Page from '../base-ui/Page';
import PhoneInputField from '../base-ui/PhoneInputField';
import PrimaryButton from '../base-ui/PrimaryButton';
import Row from '../base-ui/Row';
import Section from '../base-ui/Section';
import Spinner from '../base-ui/Spinner';
import StyledText from '../base-ui/StyledText';
import PasswordInputField from '../base-ui/PasswordInputField';
import Toast from '../base-ui/Toast';

import login from '../api/login';
import authStore from '../stores/authStore';

type State = {
  loading: boolean,
  mobileNumber: string,
  password: string,
  showPassword: boolean,
};

export default class LoginPage extends Component {
  state: State;

  constructor() {
    super();
    this.state = {
      loading: false,
      mobileNumber: '',
      password: '',
      showPassword: false,
    };
  }

  async handleLogin() {
    this.setState({ loading: true });
    const { mobileNumber, password } = this.state;
    try {
      const result = await login(`+62${mobileNumber}`, password);
      authStore.sessionToken = result.id;
      await AsyncStorage.setItem('@sessionToken', result.id);
      this.setState({ loading: false });
      Actions.reset('mainTabs');
    } catch (error) {
      this.setState({ loading: false });
      setTimeout(() => Toast.show(error.message), 500);
    }
  }

  render() {
    return (
      <Page>
        <Body contentContainerStyle={{ padding: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Login</StyledText>
            <PhoneInputField
              label="Mobile No."
              placeholder="8XXXXXXXXX"
              value={this.state.mobileNumber}
              onChangeText={mobileNumber => this.setState({ mobileNumber })}
            />
            <PasswordInputField
              secureTextEntry={!this.state.showPassword}
              label="PASSWORD"
              placeholder="Password"
              // value={this.state.password}
              onChangeText={password => this.setState({ password })}
              onShow={() => this.setState({ showPassword: !this.state.showPassword })}
            />
          </Section>
          <Section style={{ paddingTop: 24 }}>
            <PrimaryButton label="Login" onPress={() => this.handleLogin()} />
          </Section>
          <Section style={{ alignSelf: 'center', paddingTop: 16 }}>
            <Row style={{ alignSelf: 'center' }}>
              <Link label="Forgot Password" onPress={() => Actions.forgotPasswordPage()} />
              <View style={{ borderLeftWidth: 1, borderColor: '#999', marginHorizontal: 20 }} />
              <Link label="I have access code" onPress={() => Actions.reset('enterAccessCodePage')} />
            </Row>
          </Section>
        </Body>
        <Spinner visible={this.state.loading} />
      </Page>
    );
  }
}
