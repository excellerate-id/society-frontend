// @flow
import React, { Component } from 'react';
import { AsyncStorage, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react';

import Body from '../base-ui/Body';
import Col from '../base-ui/Col';
import Page from '../base-ui/Page';
import Section from '../base-ui/Section';
import Separator from '../base-ui/Separator';
import StyledText from '../base-ui/StyledText';
import TextField from '../base-ui/TextField';

import dashboardStore from '../stores/dashboardStore';

@observer
export default class AccountPage extends Component {
  static async handleLogout() {
    await AsyncStorage.setItem('@sessionToken', '');
    Actions.reset('loginPage');
  }

  render() {
    const { accountDetails } = dashboardStore;
    return (
      <Page>
        <Body contentContainerStyle={{ padding: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Account</StyledText>
          </Section>
          <Section style={{ paddingBottom: 16 }}>
            <TextField label="NAME" value={accountDetails.name} />
            <TextField label="EMAIL" value={accountDetails.email} />
            <TextField label="MOBILE NO." value={accountDetails.mobileNumber} />
            <TextField label="ADDRESS" value={accountDetails.address} />
            <TextField label="CITY" value={accountDetails.city} />
            <TextField label="PAYOUT BANK NAME" value={accountDetails.payoutBankName} />
            <TextField label="PAYOUT ACCOUNT NUMBER" value={accountDetails.payoutBankAccount} />
          </Section>
          <Separator />
          <Section>
            <TouchableOpacity
              style={{ paddingTop: 16, paddingBottom: 19 }}
              onPress={() => Actions.editProfilePage()}
            >
              <Col style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <StyledText font="OpenSans-SemiBold-14">Edit Profile</StyledText>
                <Icon name="chevron-right" color="white" size={24} />
              </Col>
            </TouchableOpacity>
            <Separator />
            <TouchableOpacity
              style={{ paddingTop: 16, paddingBottom: 19 }}
              onPress={() => Actions.changePasswordPage()}
            >
              <Col style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <StyledText font="OpenSans-SemiBold-14">Change Password</StyledText>
                <Icon name="chevron-right" color="white" size={24} />
              </Col>
            </TouchableOpacity>
          </Section>
          <Separator />
          <Section style={{ paddingVertical: 23, alignItems: 'center' }}>
            <TouchableOpacity onPress={() => AccountPage.handleLogout()}>
              <StyledText font="OpenSans-SemiBold-14-20-underline">Logout</StyledText>
            </TouchableOpacity>
          </Section>
        </Body>
      </Page>
    );
  }
}
