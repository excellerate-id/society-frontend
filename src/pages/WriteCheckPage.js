// @flow
import React, { Component } from 'react';
import { Linking } from 'react-native';

import Body from '../base-ui/Body';
import BulletNumber from '../base-ui/BulletNumber';
import Col from '../base-ui/Col';
import Header from '../base-ui/Header';
import Page from '../base-ui/Page';
import Row from '../base-ui/Row';
import SecondaryButton from '../base-ui/SecondaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';

export default class WriteCheckPage extends Component {
  render() {
    return (
      <Page>
        <Header />
        <Body contentContainerStyle={{ paddingHorizontal: 28 }}>
          <Section style={{ paddingTop: 10 }}>
            <StyledText font="Domine-Regular-28-40">Write a Check</StyledText>
          </Section>
          <Section style={{ paddingRight: 45 }}>
            <Col style={{ paddingTop: 16 }}>
              <Row>
                <Row style={{ paddingRight: 10 }}>
                  <BulletNumber label="1" />
                </Row>
                <StyledText font="OpenSans-Regular-14-20">Prepare a check with your investment amount to PT. Kelola Mitra Investama.</StyledText>
              </Row>
            </Col>
            <Col style={{ paddingTop: 16 }}>
              <Row>
                <Row style={{ paddingRight: 10 }}>
                  <BulletNumber label="2" />
                </Row>
                <StyledText font="OpenSans-Regular-14-20">Call us and we will pickup your check.</StyledText>
              </Row>
            </Col>
          </Section>
          <Section style={{ paddingTop: 36 }}>
            <SecondaryButton label="Call Us" onPress={() => Linking.openURL('tel:087780389229')} />
          </Section>
        </Body>
      </Page>
    );
  }
}
