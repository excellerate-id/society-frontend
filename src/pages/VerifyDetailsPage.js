// @flow
import React, { Component } from 'react';
import { Linking } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Actions } from 'react-native-router-flux';
import { observer } from 'mobx-react/native';

import Link from '../base-ui/Link';
import MultilineTextInputField from '../base-ui/MultilineTextInputField';
import Page from '../base-ui/Page';
import PrimaryButton from '../base-ui/PrimaryButton';
import Section from '../base-ui/Section';
import StyledText from '../base-ui/StyledText';
import TextField from '../base-ui/TextField';
import TextInputField from '../base-ui/TextInputField';

import userStore from '../stores/userStore';

@observer
export default class VerifyDetailsPage extends Component {
  render() {
    return (
      <Page>
        <KeyboardAwareScrollView contentContainerStyle={{ paddingTop: 24, paddingHorizontal: 16 }}>
          <Section>
            <StyledText font="Domine-Regular-28-40">Verify your details</StyledText>
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <TextField label="FULL NAME" value={userStore.name} />
            <TextField label="MOBILE NO." value={userStore.mobileNumber} />
            <TextInputField
              label="E-MAIL"
              value={userStore.email}
              onChangeText={(email) => { userStore.email = email; }}
            />
            <MultilineTextInputField
              label="ADDRESS"
              value={userStore.address}
              onChangeText={(address) => { userStore.address = address; }}
            />
            <TextInputField
              label="CITY"
              value={userStore.city}
              onChangeText={(city) => { userStore.city = city; }}
            />
            <TextField label="PAYOUT BANK NAME" value={userStore.payoutBankName} />
            <TextField label="PAYOUT ACCOUNT NUMBER" value={userStore.payoutBankAccount} />
          </Section>
          <Section style={{ paddingTop: 16 }}>
            <PrimaryButton label="Correct" onPress={() => Actions.enterPasswordPage()} />
          </Section>
          <Section style={{ paddingVertical: 16, alignItems: 'center' }}>
            <Link label="Incorrect name or mobile no.? Call us." onPress={() => Linking.openURL('tel:087780389229')} />
          </Section>
        </KeyboardAwareScrollView>
      </Page>
    );
  }
}
