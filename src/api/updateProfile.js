// @flow
import fetchJson from '../utils/fetchJson';

type Profile = {
  id: number,
  name: string,
  email: string,
  address: string,
  city: string,
};

export default async function updateProfile(profile: Profile) {
  try {
    const result = await fetchJson('PATCH', `users/${profile.id}`, profile);
    return result;
  } catch (error) {
    throw error;
  }
}
