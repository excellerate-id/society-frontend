// @flow
import fetchJson from '../utils/fetchJson';

import authStore from '../stores/authStore';
import userStore from '../stores/userStore';

export default async function activateUser(password: string) {
  try {
    const result = await fetchJson('PUT', 'users/activated', {
      ...userStore,
      password,
    });
    authStore.sessionToken = result.success.token;
    return result;
  } catch (error) {
    throw error;
  }
}
