// @flow
import fetchJson from '../utils/fetchJson';

export default async function resetPassword(email: string) {
  try {
    const result = await fetchJson('POST', 'users/reset', {
      email,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
