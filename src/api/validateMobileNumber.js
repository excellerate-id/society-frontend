// @flow
import fetchJson from '../utils/fetchJson';

export default async function validateMobileNumber(verificationCode: string) {
  try {
    const result = await fetchJson('POST', 'Validate/mobileNumber', {
      verificationCode,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
