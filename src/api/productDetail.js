// @flow
import fetchJson from '../utils/fetchJson';

export default async function productDetail(id: string) {
  try {
    const result = await fetchJson('GET', `product/${id}`);
    return result;
  } catch (error) {
    throw error;
  }
}
