// @flow
import fetchJson from '../utils/fetchJson';

export default async function getBankInstructions(id: number) {
  try {
    const result = await fetchJson('GET', `Bank/${id}`);
    return result;
  } catch (error) {
    throw error;
  }
}
