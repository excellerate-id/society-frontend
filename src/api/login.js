// @flow
import fetchJson from '../utils/fetchJson';

export default async function login(username: string, password: string) {
  try {
    const result = await fetchJson('POST', 'users/login', {
      username,
      password,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
