// @flow
import fetchJson from '../utils/fetchJson';

export default async function getSalesContact() {
  try {
    const result = await fetchJson('GET', 'customerServices');
    return result;
  } catch (error) {
    throw error;
  }
}
