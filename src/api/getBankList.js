// @flow
import fetchJson from '../utils/fetchJson';

export default async function getBankList(id: string) {
  try {
    const result = await fetchJson('GET', `Bank?productId=${id}`);
    return result;
  } catch (error) {
    throw error;
  }
}
