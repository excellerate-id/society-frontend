// @flow
import fetchJson from '../utils/fetchJson';

export default async function resendVerificationCode(accessCode: string) {
  try {
    const result = await fetchJson('POST', 'Validate/resendVerificationCode', {
      accessCode,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
