// @flow
import DeviceInfo from 'react-native-device-info';

import fetchJson from '../utils/fetchJson';

export default async function registerDevice() {
  try {
    const deviceInfo = {
      appVersion: DeviceInfo.getVersion(),
      buildVersion: DeviceInfo.getBuildNumber(),
      deviceId: DeviceInfo.getUniqueID(),
      userAgent: DeviceInfo.getUserAgent(),
    };
    const result = await fetchJson('POST', 'Sessions/new', deviceInfo);
    return result;
  } catch (error) {
    throw error;
  }
}
