// @flow
import fetchJson from '../utils/fetchJson';

export default async function validateAccessCode(accessCode: string) {
  try {
    const result = await fetchJson('POST', 'Validate/accessCode', {
      accessCode,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
