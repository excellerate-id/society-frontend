// @flow
import fetchJson from '../utils/fetchJson';

export default async function changePassword(oldPassword: string, newPassword: string) {
  try {
    const result = await fetchJson('POST', 'users/change-password', {
      oldPassword,
      newPassword,
    });
    return result;
  } catch (error) {
    throw error;
  }
}
