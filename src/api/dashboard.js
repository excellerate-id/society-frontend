// @flow
import fetchJson from '../utils/fetchJson';

export default async function dashboard() {
  try {
    const result = await fetchJson('GET', 'dashboards');
    return result;
  } catch (error) {
    throw error;
  }
}
