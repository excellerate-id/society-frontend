// @flow
import fetchJson from '../utils/fetchJson';

type Schedule = {
  userId: number,
  scheduledCallDate: string,
  scheduledCallTime: string,
};

export default async function createCallSchedule(schedule: Schedule) {
  try {
    const result = await fetchJson('POST', 'schedules', schedule);
    return result;
  } catch (error) {
    throw error;
  }
}
