// @flow
import { observable } from 'mobx';
import remotedev from 'mobx-remotedev';

const userStore = observable({
  accessCode: '',
  address: '',
  city: '',
  created: '',
  email: '',
  id: -1,
  mobileNumber: '',
  modified: '',
  name: '',
  photo: '',
  status: false,
});

export default remotedev(userStore, { name: 'userStore' });
