// @flow
import { observable } from 'mobx';
import remotedev from 'mobx-remotedev';

const dashboardStore = observable({
  accountDetails: {
    address: '',
    city: '',
    email: '',
    id: -1,
    mobileNumber: '',
    name: '',
  },
  productList: [],
  selectedProductId: -1,
  transactionHistory: [],
  yourInvestment: [],
});

export default remotedev(dashboardStore, { name: 'dashboardStore' });
