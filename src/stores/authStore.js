// @flow
import { observable } from 'mobx';
import remotedev from 'mobx-remotedev';

const authStore = observable({
  sessionToken: '',
});

export default remotedev(authStore, { name: 'authStore' });
