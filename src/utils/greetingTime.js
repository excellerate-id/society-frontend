// @flow
import moment from 'moment';

export default function greetingTime() {
  const hour = moment().hour();
  if (hour >= 5 && hour < 12) {
    return 'Good Morning,';
  } else if (hour >= 12 && hour < 18) {
    return 'Good Afternoon,';
  } else if (hour >= 18 && hour < 22) {
    return 'Good Evening,';
  }
  return 'Good Night,';
}
