// @flow
import numeral from 'numeral';

export default function configNumeral() {
  numeral.register('locale', 'id', {
    delimiters: {
      thousands: ',',
      decimal: '.',
    },
    abbreviations: {
      thousand: 'k',
      million: 'mio',
      billion: 'bio',
      trillion: 't',
    },
    ordinal(number) {
      return number === 1 ? 'er' : 'ème';
    },
    currency: {
      symbol: 'Rp.',
    },
  });

  numeral.locale('id');
}
