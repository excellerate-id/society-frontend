// @flow

const ID_LENGTH = 10;

export default function formatMemberId(id: number) {
  const memberId = id.toString();
  if (memberId.length < ID_LENGTH) {
    const zeros = Array((ID_LENGTH + 1) - memberId.length).join('0');
    return zeros + memberId;
  }
  return memberId;
}
