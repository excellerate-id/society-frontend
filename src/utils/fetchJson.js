// @flow
import authStore from '../stores/authStore';

type Method = 'GET' | 'POST' | 'PUT' | 'PATCH';

export default async function fetchJson(method: Method, path: string, data: any): any {
  try {
    const response = await fetch(`http://13.229.83.17/api/v1/${path}`, {
      method,
      headers: {
        Accept: 'application/json',
        Authorization: authStore.sessionToken,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    // TODO: remove if forgot password api fixed
    if (response.status === 204) {
      return {};
    }
    const result = await response.json();
    if (result.error != null) {
      throw result.error;
    } else {
      return result;
    }
  } catch (error) {
    throw error;
  }
}
