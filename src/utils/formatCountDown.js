// @flow
import moment from 'moment';

export default function formatCountDown(seconds: number) {
  const duration = moment.duration(seconds, 'seconds');
  return moment.utc(duration.asMilliseconds()).format('mm:ss');
}
