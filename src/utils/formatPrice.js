// @flow
import numeral from 'numeral';

export default function formatPrice(currency: string, amount: number) {
  return `${currency} ${numeral(amount).format('0,0 a')}`;
}
