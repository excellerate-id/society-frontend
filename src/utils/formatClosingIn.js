// @flow
import moment from 'moment';

export default function formatClosingIn(date: string) {
  const d = moment(date) - moment();
  return moment.duration(d).humanize();
}
