// @flow
import moment from 'moment';

export default function formatTransactionDate(date: string) {
  let formattedDate = moment(date).format('ddd, DD MMM YYYY');
  if (moment(date).isSame(moment(), 'day')) {
    formattedDate = 'Today';
  }
  return formattedDate;
}
