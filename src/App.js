// @flow
import React, { Component } from 'react';
import { Actions, Router, Scene, Stack, Tabs } from 'react-native-router-flux';
import Orientation from 'react-native-orientation';

import TabIcon from './base-ui/TabIcon';
import colors from './base-ui/theme/colors';

import AccountPage from './pages/AccountPage';
import ConfirmPasswordPage from './pages/ConfirmPasswordPage';
import CallUsPage from './pages/CallUsPage';
import ChangePasswordPage from './pages/ChangePasswordPage';
import ChooseBankPage from './pages/ChooseBankPage';
import EditProfilePage from './pages/EditProfilePage';
import EnterAccessCodePage from './pages/EnterAccessCodePage';
import EnterPasswordPage from './pages/EnterPasswordPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage';
import InvestmentsPage from './pages/InvestmentsPage';
import LoginPage from './pages/LoginPage';
import ProductPage from './pages/ProductPage';
import RewardsPage from './pages/RewardsPage';
import ScheduleCallPage from './pages/ScheduleCallPage';
import SplashPage from './pages/SplashPage';
import SuccessPage from './pages/SuccessPage';
import TransactionsHistoryPage from './pages/TransactionsHistoryPage';
import OffersPage from './pages/OffersPage';
import VerifyDetailsPage from './pages/VerifyDetailsPage';
import VerifyMobileNumberPage from './pages/VerifyMobileNumberPage';
import VirtualAccountDetailsPage from './pages/VirtualAccountDetailsPage';
import WriteCheckPage from './pages/WriteCheckPage';

import configNumeral from './utils/configNumeral';

configNumeral();

export default class App extends Component {
  static handleBackAndroid() {
    if (Actions.currentScene !== '_offersPage') {
      Actions.pop();
      return true;
    }
    return false;
  }

  componentDidMount() {
    Orientation.lockToPortrait();
  }

  render() {
    return (
      <Router backAndroidHandler={App.handleBackAndroid}>
        <Stack key="root" headerMode="none">
          <Scene key="splashPage" component={SplashPage} />
          <Scene key="enterAccessCodePage" component={EnterAccessCodePage} />
          <Scene key="callUsPage" component={CallUsPage} />
          <Scene key="changePasswordPage" component={ChangePasswordPage} />
          <Scene key="chooseBankPage" component={ChooseBankPage} />
          <Scene key="confirmPasswordPage" component={ConfirmPasswordPage} />
          <Scene key="editProfilePage" component={EditProfilePage} />
          <Scene key="enterPasswordPage" component={EnterPasswordPage} />
          <Scene key="forgotPasswordPage" component={ForgotPasswordPage} />
          <Scene key="loginPage" component={LoginPage} />
          <Scene key="productPage" component={ProductPage} />
          <Scene key="scheduleCallPage" component={ScheduleCallPage} />
          <Scene key="successPage" component={SuccessPage} />
          <Scene key="transactionsHistoryPage" component={TransactionsHistoryPage} />
          <Scene key="verifyDetailsPage" component={VerifyDetailsPage} />
          <Scene key="verifyMobileNumberPage" component={VerifyMobileNumberPage} />
          <Scene key="virtualAccountDetailsPage" component={VirtualAccountDetailsPage} />
          <Scene key="writeCheckPage" component={WriteCheckPage} />
          <Tabs
            key="mainTabs"
            tabBarPosition="bottom"
            activeTintColor={colors.darkGold}
            inactiveTintColor={colors.navy}
            tabBarStyle={{ height: 60 }}
            tabStyle={{ paddingVertical: 8 }}
          >
            <Scene
              key="offersPage"
              component={OffersPage}
              icon={({ tintColor }) => <TabIcon name="offers" tintColor={tintColor} />}
              tabBarLabel="Offers"
            />
            <Scene
              key="investmentsPage"
              component={InvestmentsPage}
              icon={({ tintColor }) => <TabIcon name="investments" tintColor={tintColor} />}
              tabBarLabel="Investments"
            />
            <Scene
              key="rewardsPage"
              component={RewardsPage}
              icon={({ tintColor }) => <TabIcon name="rewards" tintColor={tintColor} />}
              tabBarLabel="Rewards"
            />
            <Scene
              key="accountPage"
              component={AccountPage}
              icon={({ tintColor }) => <TabIcon name="account" tintColor={tintColor} />}
              tabBarLabel="Account"
            />
          </Tabs>
        </Stack>
      </Router>
    );
  }
}
