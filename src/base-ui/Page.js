// @flow
import React from 'react';
import { Platform, StatusBar, View } from 'react-native';

import colors from './theme/colors';

import styles from './styles/Page-styles';

type Props = {
  style?: Style,
  children?: any,
};

export default function Page(props: Props) {
  const { style, children, ...others } = props;
  return (
    <View style={[styles.container, style]} {...others}>
      <StatusBar backgroundColor={colors.navy} barStyle="light-content" />
      {Platform.OS === 'ios' ? (
        <View style={{ height: 20 }} />
      ) : null}
      {children}
    </View>
  );
}

Page.defaultProps = {
  style: null,
  children: null,
};
