// @flow
import React from 'react';
import { Image } from 'react-native';

import icOffersOn from '../images/ic_offers_on.png';
import icOffersOff from '../images/ic_offers_off.png';
import icInvestmentsOn from '../images/ic_investment_on.png';
import icInvestmentsOff from '../images/ic_investment_off.png';
import icRewardsOn from '../images/ic_rewards_on.png';
import icRewardsOff from '../images/ic_rewards_off.png';
import icAccountOn from '../images/ic_account_on.png';
import icAccountOff from '../images/ic_account_off.png';

import colors from './theme/colors';
import styles from './styles/TabIcon-styles';

type Props = {
  name: string,
  tintColor: string,
};

export default function TabIcon(props: Props) {
  const { name, tintColor } = props;
  let imageSource = null;
  if (tintColor === colors.darkGold) {
    if (name === 'offers') {
      imageSource = icOffersOn;
    } else if (name === 'investments') {
      imageSource = icInvestmentsOn;
    } else if (name === 'rewards') {
      imageSource = icRewardsOn;
    } else if (name === 'account') {
      imageSource = icAccountOn;
    }
  } else if (tintColor === colors.navy) {
    if (name === 'offers') {
      imageSource = icOffersOff;
    } else if (name === 'investments') {
      imageSource = icInvestmentsOff;
    } else if (name === 'rewards') {
      imageSource = icRewardsOff;
    } else if (name === 'account') {
      imageSource = icAccountOff;
    }
  }

  return (
    <Image
      source={imageSource}
      resizeMode="contain"
      style={styles.image}
    />
  );
}
