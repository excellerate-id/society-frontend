// @flow
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import colors from './theme/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  yr: {
    backgroundColor: 'transparent',
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 16,
    paddingRight: 4,
  },
  value: {
    backgroundColor: 'transparent',
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 28,
  },
  percentage: {
    backgroundColor: 'transparent',
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 16,
  },
});

type Props = {
  value: number,
};

export default function YearlyPercentage(props: Props) {
  const { value } = props;
  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.yr}>Yr</Text>
      </View>
      <View>
        <Text style={styles.value}>{value}<Text style={styles.percentage}>%</Text></Text>
      </View>
    </View>
  );
}
