// @flow
import React from 'react';
import { Text, View } from 'react-native';

import styles from './styles/TextField-styles';

type Props = {
  label: string,
  value: string,
};

export default function TextField(props: Props) {
  const { label, value } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  );
}
