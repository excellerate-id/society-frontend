// @flow
import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Col from './Col';
import Row from './Row';
import StyledText from './StyledText';

import styles from './styles/MenuList-styles';

type Item = {
  id: number,
  imageSource: number | { uri: string},
  label: string,
  onPress: () => void | Promise<*>,
};

type Props = {
  items: Array<Item>,
}

export default function MenuList(props: Props) {
  const { items } = props;
  return (
    <Col style={styles.container}>
      {items.map((item, index) => (
        <Col key={item.id}>
          <TouchableOpacity onPress={item.onPress}>
            <Row style={{ height: 58, alignItems: 'center' }}>
              <Col style={{ width: 60 }}>
                <Image source={item.imageSource} resizeMode="contain" style={styles.image} />
              </Col>
              <Row style={{ flex: 1, justifyContent: 'space-between', paddingLeft: 8 }}>
                <StyledText font="OpenSans-SemiBold-14-navy">{item.label}</StyledText>
                <Icon name="chevron-right" size={20} />
              </Row>
            </Row>
          </TouchableOpacity>
          {index < (items.length - 1) ? (
            <View style={styles.separator} />
          ) : null}
        </Col>
      ))}
    </Col>
  );
}
