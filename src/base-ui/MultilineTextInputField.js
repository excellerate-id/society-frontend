// @flow
import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';

import colors from './theme/colors';

import styles from './styles/MultilineTextInputField-styles';

type Props = {
  label: string,
  style?: Style,
};

type State = {
  height: number,
};

export default class MultilineTextInputField extends Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);
    this.state = {
      height: 0,
    };
    (this: any).handleContentSizeChange = this.handleContentSizeChange.bind(this);
  }

  handleContentSizeChange(e: any) {
    this.setState({ height: e.nativeEvent.contentSize.height });
  }

  render() {
    const { label, style, ...others } = this.props;
    return (
      <View style={[styles.container, { height: this.state.height + 30 }]}>
        <Text style={styles.label}>{label}</Text>
        <TextInput
          autoCorrect={false}
          multiline
          placeholderTextColor={colors.rhino}
          selectionColor={colors.white}
          underlineColorAndroid="transparent"
          style={[styles.textInput, { height: this.state.height + 5 }]}
          onContentSizeChange={this.handleContentSizeChange}
          {...others}
        />
      </View>
    );
  }
}

// $FlowFixMe
MultilineTextInputField.defaultProps = {
  style: null,
};
