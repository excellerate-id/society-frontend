// @flow
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import colors from './theme/colors';

const BUTTON_SIZE = 43;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.lightGold,
    borderRadius: BUTTON_SIZE / 2,
    height: BUTTON_SIZE,
    justifyContent: 'center',
    width: BUTTON_SIZE,
  },
  icon: {
    backgroundColor: 'transparent',
  },
});

type Props = {
  name: string,
  onPress: () => void,
};

export default function IconButton(props: Props) {
  const { name, onPress } = props;
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Icon name={name} color="white" size={24} style={styles.icon} />
      </View>
    </TouchableOpacity>
  );
}
