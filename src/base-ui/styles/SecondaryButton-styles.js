// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: colors.white,
    borderRadius: 28,
    borderWidth: 1,
    justifyContent: 'center',
    height: 56,
    paddingHorizontal: 16,
  },
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 18,
  },
});

export default styles;
