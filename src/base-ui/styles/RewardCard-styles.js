// @flow
import { Dimensions, StyleSheet } from 'react-native';

import colors from '../theme/colors';

const { width: screenWidth } = Dimensions.get('window');
const cardWidth = screenWidth - 40;
const cardHeight = screenWidth / 1.7;
export const cardBorderRadius = screenWidth > 320 ? 3 : 6;

const styles = StyleSheet.create({
  container: {
    width: cardWidth,
    height: cardHeight,
    padding: 16,
  },
  title: {
    backgroundColor: 'transparent',
    color: colors.darkGold,
    fontFamily: 'Didot-Bold',
    fontSize: 27,
  },
  name: {
    backgroundColor: 'transparent',
    color: colors.navy,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
  },
  description: {
    backgroundColor: 'transparent',
    color: colors.navy,
    fontFamily: 'OpenSans-Italic',
    fontSize: 11,
  },
  id: {
    backgroundColor: 'transparent',
    color: colors.navy,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  number: {
    backgroundColor: 'transparent',
    color: colors.navy,
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
  },
});

export default styles;
