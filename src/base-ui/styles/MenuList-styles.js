// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    borderRadius: 4,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  item: {
    alignItems: 'center',
    borderColor: colors.oxfordBlue,
    height: 58,
  },
  image: {
    height: 30,
    width: 60,
  },
  separator: {
    borderBottomWidth: 1,
    borderColor: colors.oxfordBlue,
  },
});

export default styles;
