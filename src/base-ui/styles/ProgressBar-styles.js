// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.heather,
    borderRadius: 12,
    flexDirection: 'row',
    height: 12,
  },
  value: {
    backgroundColor: colors.limedSpruce,
    borderRadius: 12,
  },
});

export default styles;
