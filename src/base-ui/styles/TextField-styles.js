// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    marginVertical: 8,
  },
  label: {
    color: colors.heather,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  value: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    lineHeight: 24,
  },
});

export default styles;
