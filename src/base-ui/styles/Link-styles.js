// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  disabled: {
    color: colors.dustyGray,
  },
});

export default styles;
