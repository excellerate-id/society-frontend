// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  box: {
    borderColor: colors.white,
    borderBottomWidth: 1,
    height: 40,
    marginHorizontal: 4,
    width: 40,
  },
  textInput: {
    color: colors.white,
    fontSize: 24,
    height: 40,
    padding: 0,
    textAlign: 'center',
  },
});

export default styles;
