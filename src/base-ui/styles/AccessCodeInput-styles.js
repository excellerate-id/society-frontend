// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    borderColor: colors.dustyGray,
    borderBottomWidth: 1,
    height: 40,
  },
  textInput: {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 24,
    height: 40,
    lineHeight: 24,
    padding: 0,
  },
});

export default styles;
