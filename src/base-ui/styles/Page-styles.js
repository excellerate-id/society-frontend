// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.navy,
    flex: 1,
  },
});

export default styles;
