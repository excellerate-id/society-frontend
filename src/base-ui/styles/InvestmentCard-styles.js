// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    borderRadius: 4,
    overflow: 'hidden',
  },
  imageBackground: {
    height: 162,
    flexDirection: 'row',
  },
  name: {
    backgroundColor: 'transparent',
    bottom: 10,
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 14,
    left: 10,
    position: 'absolute',
  },
  label: {
    color: colors.black,
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    paddingBottom: 5,
  },
  value: {
    color: colors.black,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
  },
});

export default styles;
