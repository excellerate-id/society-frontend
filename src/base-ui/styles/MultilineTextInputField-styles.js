// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    borderColor: colors.dustyGray,
    borderBottomWidth: 1,
    marginVertical: 8,
    paddingBottom: 8,
  },
  label: {
    color: colors.heather,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
    paddingBottom: 3,
  },
  textInput: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    lineHeight: 24,
    padding: 0,
  },
});

export default styles;
