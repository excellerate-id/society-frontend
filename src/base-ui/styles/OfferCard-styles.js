// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  name: {
    backgroundColor: 'transparent',
    bottom: 10,
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 14,
    left: 10,
    position: 'absolute',
  },
  label: {
    color: colors.navy,
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    paddingBottom: 5,
  },
  offerLabel: {
    color: colors.navy,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  offerTime: {
    color: colors.navy,
    fontFamily: 'OpenSans-Regular',
    fontSize: 11,
  },
  offerTagView: {
    backgroundColor: colors.white,
    height: 42,
    width: 87,
    paddingLeft: 8,
    justifyContent: 'center',
  },
});

export default styles;
