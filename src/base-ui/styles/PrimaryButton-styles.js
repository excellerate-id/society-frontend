// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.lightGold,
    borderRadius: 28,
    justifyContent: 'center',
    height: 56,
  },
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
  },
  disabled: {
    backgroundColor: colors.dustyGray,
  },
});

export default styles;
