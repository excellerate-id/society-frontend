// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  icon: {
    backgroundColor: 'transparent',
    color: colors.white,
  },
  closingTimer: {
    color: colors.navy,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  label: {
    color: colors.navy,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    paddingBottom: 5,
  },
  value: {
    color: colors.navy,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    paddingBottom: 5,
  },
  investedLabel: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    paddingBottom: 5,
  },
  investedValue: {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 16,
    lineHeight: 24,
  },
});

export default styles;
