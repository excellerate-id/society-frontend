// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGold,
    height: 56,
    flexDirection: 'row',
  },
  button: {
    alignItems: 'center',
    backgroundColor: colors.lightGold,
    height: 56,
    justifyContent: 'center',
    flex: 1,
  },
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
  },
  verticalLine: {
    alignSelf: 'center',
    borderColor: colors.white,
    borderWidth: 1,
    height: 40,
  },
});

export default styles;
