// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.lightGold,
    borderRadius: 24 / 2,
    height: 24,
    justifyContent: 'center',
    width: 24,
  },
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 12,
  },
});

export default styles;
