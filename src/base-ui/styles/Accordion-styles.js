// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-between',
  },
  label: {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
  },
});

export default styles;
