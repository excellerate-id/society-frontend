// @flow
import { StyleSheet } from 'react-native';

import colors from '../theme/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 64,
    paddingHorizontal: 16,
  },
  backButton: {
    height: 48,
    justifyContent: 'center',
    width: 48,
  },
  icon: {
    color: colors.white,
  },
  title: {
    flex: 1,
    alignItems: 'center',
  },
  rightButton: {
    height: 48,
    justifyContent: 'center',
    width: 48,
  },
});

export default styles;
