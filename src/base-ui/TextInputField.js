// @flow
import React from 'react';
import { Text, TextInput, View } from 'react-native';

import colors from './theme/colors';
import styles from './styles/TextInputField-styles';

type Props = {
  label: string,
  style?: Style,
};

export default function TextInputField(props: Props) {
  const { label, style, ...others } = props;
  const containerStyles = [styles.container];
  return (
    <View style={containerStyles}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        autoCorrect={false}
        placeholderTextColor={colors.rhino}
        selectionColor={colors.white}
        underlineColorAndroid="transparent"
        style={[styles.textInput, style]}
        {...others}
      />
    </View>
  );
}

TextInputField.defaultProps = {
  style: null,
};
