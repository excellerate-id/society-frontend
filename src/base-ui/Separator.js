// @flow
import React from 'react';
import { StyleSheet, View } from 'react-native';

import colors from './theme/colors';

const styles = StyleSheet.create({
  container: {
    borderColor: colors.white,
    borderBottomWidth: 0.5,
  },
});

export default function Separator() {
  return (
    <View style={styles.container} />
  );
}
