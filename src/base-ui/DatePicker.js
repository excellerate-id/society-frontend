// @flow
import React from 'react';
import { Modal, StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import { Calendar } from 'react-native-calendars';

import colors from './theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 16,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  content: {
    backgroundColor: colors.white,
    height: 320,
  },
});

export default function DatePicker(props: any) {
  const { visible, onRequestClose, ...others } = props;
  return (
    <Modal animationType="fade" transparent visible={visible} onRequestClose={onRequestClose}>
      <TouchableWithoutFeedback onPress={onRequestClose}>
        <View style={styles.container} />
      </TouchableWithoutFeedback>
      <View style={styles.content}>
        <Calendar
          theme={{ arrowColor: colors.navy, selectedDayBackgroundColor: colors.darkGold }}
          {...others}
        />
      </View>
      <TouchableWithoutFeedback onPress={onRequestClose}>
        <View style={styles.container} />
      </TouchableWithoutFeedback>
    </Modal>
  );
}
