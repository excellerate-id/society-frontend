// @flow
import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';

import styles from './styles/FooterButton-styles';

type Props = {
  disabled: boolean,
  onCallUs: () => void | Promise<*>,
  onInvest: () => void | Promise<*>,
};

export default function FooterButton(props: Props) {
  const { disabled = false, onCallUs, onInvest } = props;
  return (
    <View style={styles.container}>
      <TouchableOpacity disabled={disabled} style={styles.button} onPress={onCallUs}>
        <Text style={styles.label}>Call Us</Text>
      </TouchableOpacity>
      <View style={styles.verticalLine} />
      <TouchableOpacity disabled={disabled} style={styles.button} onPress={onInvest}>
        <Text style={styles.label}>Invest</Text>
      </TouchableOpacity>
    </View>
  );
}
