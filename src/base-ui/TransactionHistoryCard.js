// @flow
import React from 'react';
import { View, Text, Image } from 'react-native';
import Moment from 'moment';

import colors from './theme/colors';
import moneyIn from '../images/ic_moneyin.png';
import moneyOut from '../images/ic_moneyout.png';


type Props = {
  amount: string,
  title: string,
  type: string,
  created: string,
};

export default function TransactionHistoryCard(props: Props) {
  const { amount, title, type, created } = props;

  return (
    <View style={{ height: 76, backgroundColor: colors.white, borderRadius: 4, overflow: 'hidden' }}>
      <View style={{ flex: 1, flexDirection: 'row', borderRadius: 4 }}>
        <View style={{ flex: 0.25, justifyContent: 'center', alignItems: 'center' }}>
          {
            type === 'Invest' ? (
              <Image
                source={moneyIn}
                style={{ height: 40, width: 40, backgroundColor: 'transparent' }}
              />
            ) : (
              <Image
                source={moneyOut}
                style={{ height: 40, width: 40, backgroundColor: 'transparent' }}
              />
            )
          }
        </View>

        <View style={{ flex: 1, paddingRight: 16 }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 2 }}>
            <View style={{ justifyContent: 'flex-end' }}>
              <Text style={{ fontFamily: 'OpenSans-SemiBold', fontSize: 14, color: colors.navy }}>{type}</Text>
            </View>

            <View style={{ justifyContent: 'flex-end' }}>
              <Text style={{ fontFamily: 'OpenSans-Regular', fontSize: 14, color: colors.navy }}>{amount}</Text>
            </View>
          </View>

          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', paddingTop: 2 }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontFamily: 'OpenSans-Regular', fontSize: 11, color: colors.navy }}>{title}</Text>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Text style={{ fontFamily: 'OpenSans-Regular', fontSize: 11, color: colors.dustyGray }}>
                {Moment(created).format('HH:mm')}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
