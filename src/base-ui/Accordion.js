// @flow
import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from './styles/Accordion-styles';

type Props = {
  label: string,
  children: any,
  collapsed: boolean,
};

type State = {
  collapsed: boolean,
};

export default class Accordion extends Component {
  props: Props;
  state: State;

  constructor(props: Props) {
    super(props);
    this.state = {
      collapsed: props.collapsed,
    };
    (this: any).toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ collapsed: !this.state.collapsed });
  }

  render() {
    const { collapsed } = this.state;
    return (
      <View>
        <TouchableOpacity onPress={() => this.toggle()}>
          <View style={styles.header}>
            <Text style={styles.label}>{this.props.label}</Text>
            <Icon
              color="white"
              name={collapsed ? 'chevron-down' : 'chevron-up'}
              size={24}
            />
          </View>
        </TouchableOpacity>
        <Collapsible collapsed={collapsed}>
          {this.props.children}
        </Collapsible>
      </View>
    );
  }
}
