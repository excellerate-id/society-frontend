// @flow
import React from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Actions } from 'react-native-router-flux';

import StyledText from './StyledText';

import styles from './styles/Header-styles';

type Props = {
  title?: string,
  onBack?: () => void,
};

export default function Header(props: Props) {
  const { title, onBack } = props;
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={onBack}>
        <View style={styles.backButton}>
          <Icon name="arrow-left" size={24} style={styles.icon} />
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.title}>
        <StyledText font="OpenSans-SemiBold-18-26">{title}</StyledText>
      </View>
      <View style={styles.rightButton} />
    </View>
  );
}

Header.defaultProps = {
  title: '',
  onBack: () => Actions.pop(),
};
