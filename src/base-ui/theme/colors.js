// @flow

export default {
  black: '#000000',
  darkGold: '#A28C52',
  dustyGray: '#999999',
  heather: '#BBC0CE',
  lightGold: '#CEB982',
  limedSpruce: '#3D4855',
  navy: '#001A3E',
  pickledBlueWood: '#32445C',
  rhino: '#2F4660',
  white: '#FFFFFF',
  pastelGreen: '#93D864',
  silver: '#CCCCCC',
  gullGray: '#A1ACB9',
  mischka: '#D3D5DB',
  shuttleGray: '#5E6678',
  oxfordBlue: '#333F4C',
  bermudaGray: '#7686AA',
};
