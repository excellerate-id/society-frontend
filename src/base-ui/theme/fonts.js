// @flow
import colors from './colors';

export default {
  'Didot-Bold-36': {
    color: colors.lightGold,
    fontFamily: 'Didot-Bold',
    fontSize: 36,
  },
  'Domine-Regular-28-40': {
    color: colors.white,
    fontFamily: 'Domine-Regular',
    fontSize: 28,
    lineHeight: 40,
  },
  'Lato-Bold-11': {
    color: colors.mischka,
    fontFamily: 'Lato-Bold',
    fontSize: 11,
  },
  'Lato-Bold-14': {
    color: colors.white,
    fontFamily: 'Lato-Bold',
    fontSize: 14,
  },
  'Lato-Bold-14-black': {
    color: colors.black,
    fontFamily: 'Lato-Bold',
    fontSize: 14,
  },
  'Lato-Bold-14-underline': {
    color: colors.white,
    fontFamily: 'Lato-Bold',
    fontSize: 14,
    textDecorationLine: 'underline',
  },
  'Lato-Bold-16': {
    color: colors.white,
    fontFamily: 'Lato-Bold',
    fontSize: 16,
  },
  'Lato-Bold-30': {
    color: colors.white,
    fontFamily: 'Lato-Bold',
    fontSize: 30,
  },
  'Lato-Italic-13': {
    color: colors.white,
    fontFamily: 'Lato-Italic',
    fontSize: 13,
  },
  'Lato-Italic-16-20': {
    color: colors.white,
    fontFamily: 'Lato-Italic',
    fontSize: 16,
    lineHeight: 20,
  },
  'Lato-Regular-14': {
    color: colors.white,
    fontFamily: 'Lato-Regular',
    fontSize: 14,
  },
  'Lato-Regular-14-20': {
    color: colors.white,
    fontFamily: 'Lato-Regular',
    fontSize: 14,
    lineHeight: 24,
  },
  'Lato-Regular-16': {
    color: colors.white,
    fontFamily: 'Lato-Regular',
    fontSize: 16,
  },
  'Lato-Regular-16-black': {
    color: colors.black,
    fontFamily: 'Lato-Regular',
    fontSize: 16,
  },
  'Lato-Regular-18': {
    color: colors.white,
    fontFamily: 'Lato-Regular',
    fontSize: 18,
  },
  'OpenSans-Bold-11': {
    color: colors.heather,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  'OpenSans-Bold-11-white': {
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 11,
  },
  'OpenSans-Bold-14-20': {
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 14,
    lineHeight: 20,
  },
  'OpenSans-Bold-16': {
    color: colors.white,
    fontFamily: 'OpenSans-Bold',
    fontSize: 16,
  },
  'OpenSans-Italic-14-20': {
    color: colors.white,
    fontFamily: 'OpenSans-Italic',
    fontSize: 14,
    lineHeight: 20,
  },
  'OpenSans-Italic-16-20': {
    color: colors.white,
    fontFamily: 'OpenSans-Italic',
    fontSize: 16,
    lineHeight: 20,
  },
  'OpenSans-Regular-12-20': {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 12,
    lineHeight: 20,
  },
  'OpenSans-Regular-14-20': {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 14,
    lineHeight: 20,
  },
  'OpenSans-Regular-16-24': {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 16,
    lineHeight: 24,
  },
  'OpenSans-Regular-24-30': {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 24,
    lineHeight: 30,
  },
  'OpenSans-SemiBold-12': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 12,
  },
  'OpenSans-SemiBold-14': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
  },
  'OpenSans-SemiBold-14-20': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    lineHeight: 20,
  },
  'OpenSans-SemiBold-14-20-underline': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    lineHeight: 20,
    textDecorationLine: 'underline',
  },
  'OpenSans-SemiBold-14-navy': {
    color: colors.navy,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 14,
    lineHeight: 20,
  },
  'OpenSans-SemiBold-16-24': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    lineHeight: 24,
  },
  'OpenSans-SemiBold-16-24-underline': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
    lineHeight: 24,
    textDecorationLine: 'underline',
  },
  'OpenSans-SemiBold-18': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
  },
  'OpenSans-SemiBold-18-26': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 18,
    lineHeight: 26,
  },
  'OpenSans-SemiBold-28': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 28,
  },
  'OpenSans-SemiBoldItalic-18-26': {
    color: colors.white,
    fontFamily: 'OpenSans-SemiBoldItalic',
    fontSize: 18,
    lineHeight: 26,
  },
};
