// @flow
import React, { Component } from 'react';
import { Button, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';

import colors from './theme/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    height: 45,
    justifyContent: 'center',
    paddingLeft: 15,
  },
});

type Props = {
  value: string,
  onChangeValue: () => void,
};

type State = {
  visible: boolean,
};

export default class ModalPicker extends Component {
  props: Props;
  state: State;

  constructor() {
    super();
    this.state = {
      visible: false,
    };
  }

  toggle() {
    this.setState({ visible: !this.state.visible });
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.toggle()}>
          <Text>ModalPicker</Text>
        </TouchableOpacity>
        <Modal animationType="fade" transparent visible={this.state.visible} onRequestClose={() => {}}>
          <View style={{ flex: 1, justifyContent: 'center', padding: 16, backgroundColor: 'rgba(0, 0, 0, 0.8)' }}>
            <View style={{ backgroundColor: 'white' }}>
              <Text>Modal</Text>
              <Button title="close" onPress={() => this.toggle()} />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
