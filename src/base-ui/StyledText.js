// @flow
import React from 'react';
import { Text } from 'react-native';

import fonts from './theme/fonts';

import styles from './styles/StyledText-styles';

type Props = {
  font: $Keys<typeof fonts>,
  style?: Style,
};

export default function StyledText(props: Props) {
  const { font, style, ...others } = props;
  const fontStyle = fonts[font];
  return <Text style={[fontStyle, styles.container, style]} {...others} />;
}

StyledText.defaultProps = {
  style: null,
};
