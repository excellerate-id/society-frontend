// @flow
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';

import bgMemberCard from '../images/bg_membercard.jpg';
import styles, { cardBorderRadius } from './styles/RewardCard-styles';
import formatMemberId from '../utils/formatMemberId';

type Props = {
  account: {
    id: number,
    name: string,
    level: string,
  },
};

export default function RewardCard(props: Props) {
  const { account } = props;
  return (
    <ImageBackground
      source={bgMemberCard}
      borderRadius={cardBorderRadius}
      resizeMode="stretch"
      style={styles.container}
    >
      <View style={{ flex: 1, alignItems: 'flex-end' }}>
        <Text fontStyle="logoTextSplash" style={styles.title}>SOCIETY 51</Text>
      </View>
      <View style={{ flex: 1, justifyContent: 'flex-end' }}>
        <View style={{ paddingBottom: 12 }}>
          <Text style={styles.name}>{account.name}</Text>
          <Text style={styles.description}>{account.level}</Text>
        </View>
        <View>
          <Text style={styles.id}>MEMBER ID</Text>
          <Text style={styles.number}>{formatMemberId(account.id)}</Text>
        </View>
      </View>
    </ImageBackground>
  );
}
