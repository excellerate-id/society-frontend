// @flow
import React from 'react';
import { Text, TextInput, View } from 'react-native';

import colors from './theme/colors';
import styles from './styles/PasswordInputField-styles';

type Props = {
  label: string,
  style?: Style,
  onShow?: () => void,
};

export default function PasswordInputField(props: Props) {
  const { label, style, onShow, ...others } = props;
  const containerStyles = [styles.container];
  return (
    <View style={containerStyles}>
      <View style={{ flexDirection: 'column', flex: 1 }}>
        <Text style={styles.label}>{label}</Text>
        <TextInput
          autoCorrect={false}
          placeholderTextColor={colors.rhino}
          selectionColor={colors.white}
          underlineColorAndroid="transparent"
          style={[styles.textInput, style]}
          {...others}
        />
      </View>
      {onShow ? (
        <View style={{ top: 21 }}>
          <Text style={styles.showPassword} onPress={onShow}>SHOW</Text>
        </View>
      ) : null}
    </View>
  );
}

PasswordInputField.defaultProps = {
  style: null,
  onShow: null,
};
