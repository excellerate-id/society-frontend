// @flow
import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import styles from './styles/Link-styles';

type Props = {
  disabled?: boolean,
  label: string,
  onPress: () => void | Promise<*>,
};

export default function Link(props: Props) {
  const { disabled, label, onPress } = props;
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <Text style={[styles.label, disabled && styles.disabled]}>{label}</Text>
    </TouchableOpacity>
  );
}

Link.defaultProps = {
  disabled: false,
};
