// @flow
import React from 'react';
import { Dimensions, StyleSheet, ImageBackground } from 'react-native';

import backgroundImage from '../images/bg_splash.jpg';

const { height: screenHeight, width: screenWidth } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    height: screenHeight,
    flex: 1,
    width: screenWidth,
  },
});

type Props = {
  style?: Style,
};

export default function PageBackground(props: Props) {
  const { style, ...others } = props;
  return (
    <ImageBackground
      source={backgroundImage}
      style={[styles.container, style]}
      {...others}
    />
  );
}

PageBackground.defaultProps = {
  style: null,
};
