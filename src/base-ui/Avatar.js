// @flow
import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import colors from './theme/colors';

const IMAGE_SIZE = 48;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderRadius: IMAGE_SIZE / 2,
    height: IMAGE_SIZE,
    justifyContent: 'center',
    width: IMAGE_SIZE,
  },
  image: {
    borderRadius: IMAGE_SIZE / 2,
    height: IMAGE_SIZE,
    width: IMAGE_SIZE,
  },
});

type Props = {
  image: string,
};

export default function Avatar(props: Props) {
  const { image } = props;
  return (
    <View style={styles.container}>
      {image == null ? (
        <Icon name="account" size={30} color={colors.navy} />
      ) : (
        <Image source={{ uri: image }} style={styles.image} />
      )}
    </View>
  );
}
