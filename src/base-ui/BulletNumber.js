// @flow
import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles/BulletNumber-styles';

type Props = {
  label: string,
};

export default function BulletNumber(props: Props) {
  const { label } = props;
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
    </View>
  );
}
