// @flow
import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';

import styles from './styles/SecondaryButton-styles';

type Props = {
  inline?: boolean,
  label: string,
  small?: boolean,
  onPress: () => any,
};

export default function SecondaryButton(props: Props) {
  const { inline = false, label, small = false, onPress } = props;
  const containerStyle = [styles.container];
  const labelStyle = [styles.label];
  if (inline) {
    containerStyle.push({ alignSelf: 'center' });
  }
  if (small) {
    containerStyle.push({ height: 36 });
    labelStyle.push({ fontSize: 14 });
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={containerStyle}>
        <Text style={labelStyle}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

SecondaryButton.defaultProps = {
  inline: false,
  small: false,
};
