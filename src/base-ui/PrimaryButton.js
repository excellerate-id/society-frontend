// @flow
import React from 'react';
import { TouchableOpacity, Text, View } from 'react-native';

import styles from './styles/PrimaryButton-styles';

type Props = {
  label: string,
  onPress: () => void | Promise<*>,
  disabled?: boolean,
};

export default function PrimaryButton(props: Props) {
  const { label, onPress, disabled } = props;
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress}>
      <View style={[styles.container, disabled && styles.disabled]}>
        <Text style={styles.label}>{label}</Text>
      </View>
    </TouchableOpacity>
  );
}

PrimaryButton.defaultProps = {
  disabled: false,
};
