// @flow
import React, { Component } from 'react';
import { TextInput, View } from 'react-native';

import styles from './styles/DigitInput-styles';
import colors from './theme/colors';

type Props = {
  digits: Array<string>,
  onChangeDigits: (digits: Array<string>) => void | Promise<*>,
};

type State = {
  selectedIndex: number,
};

export default class DigitInput extends Component {
  props: Props;
  state: State;
  digitRefs: Array<TextInput>;

  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
    };
    this.digitRefs = [];
  }

  componentDidMount() {
    this.digitRefs[0].focus();
  }

  handleOnChangeText(index: number, digit: string) {
    const { digits } = this.props;
    digits[index] = digit;
    this.props.onChangeDigits(digits);
    if (index < (digits.length - 1)) {
      this.digitRefs[index + 1].focus();
    }
    if (index === (digits.length - 1)) {
      this.digitRefs[index].blur();
    }
  }

  handleOnFocus(index: number) {
    this.setState({ selectedIndex: index });
    if (this.props.digits[index] !== -1) {
      const newDigits = this.props.digits.map((digit, i) => {
        if (i >= index) {
          return '';
        }
        return digit;
      });
      this.props.onChangeDigits(newDigits);
    }
  }

  render() {
    const { digits } = this.props;
    const { selectedIndex } = this.state;
    return (
      <View style={styles.container}>
        {digits.map((digit, index) => (
          <View
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            style={[
              styles.box,
              { borderColor: selectedIndex === index ? colors.lightGold : colors.white },
            ]}
          >
            <TextInput
              ref={ref => this.digitRefs.push(ref)}
              keyboardType="numeric"
              maxLength={1}
              selectTextOnFocus
              style={styles.textInput}
              underlineColorAndroid="transparent"
              value={digit}
              onChangeText={text => this.handleOnChangeText(index, text)}
              onFocus={() => this.handleOnFocus(index)}
            />
          </View>
        ))}
      </View>
    );
  }
}
