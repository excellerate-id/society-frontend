// @flow
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import ProgressBar from './ProgressBar';
import YearlyPercentage from './YearlyPercentage';
import colors from './theme/colors';
import styles from './styles/OfferCard-styles';

import formatPrice from '../utils/formatPrice';
import formatClosingIn from '../utils/formatClosingIn';

type Product = {
  title: string,
  interestRate: number,
  remaining: number,
  currency: string,
  type: string,
  maturityAt: string,
  startAt: string,
  closingAt: string,
  tenure: number,
  repaymentDate: number,
  collateral: string,
  totalQuantum: number,
  minInvestment: number,
  maxInvestment: number,
  description: string,
  image: number | { uri: string },
  // onPress: () => void,
}

type Props = {
  product: Product,
}

export default function OfferCard(props: Props) {
  const { product } = props;
  const { totalQuantum, remaining } = product;
  let percentageRate = 0;
  if (totalQuantum >= remaining && totalQuantum > 0) {
    percentageRate = Math.floor(((totalQuantum - remaining) / totalQuantum) * 100);
  }

  return (
    <View style={{ backgroundColor: colors.white, borderRadius: 4, overflow: 'hidden' }}>
      <ImageBackground
        source={{ uri: product.image }}
        style={{ height: 162, flexDirection: 'row' }}
        borderTopLeftRadius={4}
        borderTopRightRadius={4}
      >
        <LinearGradient
          colors={['transparent', colors.black]}
          style={{ position: 'absolute', top: 80, bottom: 0, left: 0, right: 0 }}
        />
        <View style={{ flex: 1, flexDirection: 'column' }}>
          <View style={{ flex: 1, paddingTop: 18 }}>
            {product.closingAt != null ? (
              <View style={styles.offerTagView}>
                <Text style={styles.offerLabel}>Closing In</Text>
                <Text style={styles.offerTime}>{formatClosingIn(product.closingAt)}</Text>
              </View>
            ) : null}
          </View>

          <View style={{ flex: 1 }}>
            <Text style={styles.name}>{product.title}</Text>
          </View>
        </View>

        <View style={{ width: 100, justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 }}>
          {product.interestRate ? (
            <YearlyPercentage value={product.interestRate} />
          ) : null}
        </View>
      </ImageBackground>

      <View style={{ height: 59 }}>
        <View style={{ flex: 1, paddingHorizontal: 10 }}>
          <View style={{ paddingVertical: 10 }}>
            <ProgressBar percentage={percentageRate} />
          </View>

          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.label}>{percentageRate}% filled</Text>
            <Text style={styles.label}>
              {formatPrice(product.currency, product.remaining)} remaining
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}
