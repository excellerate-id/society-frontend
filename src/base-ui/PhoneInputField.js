// @flow
import React from 'react';
import { Text, TextInput, View } from 'react-native';

import colors from './theme/colors';
import styles from './styles/PhoneInputField-styles';

type Props = {
  label: string,
  style?: Style,
};

export default function PhoneInputField(props: Props) {
  const { label, style, ...others } = props;
  const containerStyles = [styles.container];
  return (
    <View style={containerStyles}>
      <Text style={styles.label}>{label}</Text>
      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ width: 40 }}>
          <TextInput
            editable={false}
            placeholder="+62"
            placeholderTextColor={colors.white}
            underlineColorAndroid="transparent"
            style={styles.textInput}
          />
        </View>
        <View style={{ flex: 1 }}>
          <TextInput
            autoCorrect={false}
            keyboardType="phone-pad"
            placeholderTextColor={colors.rhino}
            selectionColor={colors.white}
            underlineColorAndroid="transparent"
            style={[styles.textInput, style]}
            {...others}
          />
        </View>
      </View>
    </View>
  );
}

PhoneInputField.defaultProps = {
  style: null,
};
