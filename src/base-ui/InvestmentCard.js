// @flow
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import YearlyPercentage from './YearlyPercentage';
import colors from './theme/colors';
import styles from './styles/InvestmentCard-styles';

type Props = {
  image: string,
  title: string,
  rate: number,
  amount: string,
  nextRepayment: string,
}

export default function InvestmentCard(props: Props) {
  const { image, title, rate, amount, nextRepayment } = props;
  return (
    <View style={styles.container}>
      <ImageBackground
        source={{ uri: image }}
        style={styles.imageBackground}
        borderTopLeftRadius={4}
        borderTopRightRadius={4}
      >
        <LinearGradient
          colors={['transparent', colors.black]}
          style={{ position: 'absolute', top: 75, bottom: 0, left: 0, right: 0 }}
        />
        <View style={{ flex: 1 }}>
          <Text style={styles.name}>{title}</Text>
        </View>
        <View style={{ width: 100, justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 }}>
          {rate == null ? null : (
            <YearlyPercentage value={rate} />
          )}
        </View>
      </ImageBackground>
      <View style={{ flexDirection: 'row', height: 75, paddingHorizontal: 16 }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
          <Text style={styles.label}>Your investment</Text>
          <Text style={styles.value}>{amount}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end' }}>
          {nextRepayment == null ? null : (
            <View>
              <Text style={styles.label}>Next Repayment</Text>
              <Text style={styles.value}>{nextRepayment}</Text>
            </View>
          )}
        </View>
      </View>
    </View>
  );
}
