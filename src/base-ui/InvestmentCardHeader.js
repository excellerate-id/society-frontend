// @flow
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';

import ProgressBar from './ProgressBar';
import YearlyPercentage from './YearlyPercentage';

import colors from './theme/colors';
import styles from './styles/InvestmentCardHeader-styles';

import formatPrice from '../utils/formatPrice';
import formatClosingIn from '../utils/formatClosingIn';

type Props = {
  invested: boolean,
  amount: number,
  product: Object,
};

export default function InvestmentCardHeader(props: Props) {
  const { invested, amount, product } = props;
  const { totalQuantum, remaining } = product;
  let percentageRate = 0;
  if (totalQuantum >= remaining && totalQuantum > 0) {
    percentageRate = Math.floor(((totalQuantum - remaining) / totalQuantum) * 100);
  }

  return (
    <View style={{ backgroundColor: colors.white }}>
      <ImageBackground
        source={{ uri: product.image }}
        style={{ height: 162, flexDirection: 'row' }}
      >
        <LinearGradient
          colors={['transparent', colors.black]}
          style={{ position: 'absolute', top: 80, bottom: 0, left: 0, right: 0 }}
        />
        <View style={{ flex: 1 }} />
        <View style={{ width: 100, justifyContent: 'flex-end', alignItems: 'flex-end', padding: 10 }}>
          {product.interestRate ? (
            <YearlyPercentage value={product.interestRate} />
          ) : null}
        </View>
        {product.closingAt != null ? (
          <View style={{ position: 'absolute', top: 10, right: 1, padding: 6, backgroundColor: colors.white }}>
            <Text style={styles.closingTimer}>Closing in {formatClosingIn(product.closingAt)}</Text>
          </View>
        ) : null}
      </ImageBackground>

      {invested ? (
        <View style={{ backgroundColor: colors.navy, flexDirection: 'row', height: 75, paddingHorizontal: 16 }}>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start' }}>
            <Text style={styles.investedLabel}>Total Investment</Text>
            <Text style={styles.investedValue}>{formatPrice(product.currency, amount)}</Text>
          </View>
        </View>
      ) : (
        <View style={{ height: 59 }}>
          <View style={{ flex: 1, paddingHorizontal: 10 }}>
            <View style={{ paddingVertical: 10 }}>
              <ProgressBar percentage={percentageRate} />
            </View>

            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.label}>{percentageRate}% filled</Text>
              <Text style={styles.value}>
                {formatPrice(product.currency, product.remaining)} remaining
              </Text>
            </View>
          </View>
        </View>
      )}
    </View>
  );
}

InvestmentCardHeader.defaultProps = {
  onBack: () => Actions.pop(),
};
