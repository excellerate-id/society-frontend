// @flow
import React from 'react';
import { TextInput, View } from 'react-native';

import colors from './theme/colors';

import styles from './styles/AccessCodeInput-styles';

type Props = {
  value: string,
  onChangeText: (text: string) => void,
};

export default function AccessCodeInput(props: Props) {
  const { value, onChangeText } = props;
  return (
    <View style={styles.container}>
      <TextInput
        autoCapitalize="characters"
        autoCorrect={false}
        selectionColor={colors.white}
        underlineColorAndroid="transparent"
        style={styles.textInput}
        value={value}
        onChangeText={onChangeText}
      />
    </View>
  );
}
