// @flow
import React, { Component } from 'react';
import { Clipboard, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Toast from './Toast';
import colors from './theme/colors';

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  label: {
    color: colors.heather,
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 16,
  },
  box: {
    borderColor: colors.white,
    borderRadius: 3,
    borderWidth: 1,
    marginTop: 4,
    padding: 6,
  },
  value: {
    color: colors.white,
    fontFamily: 'OpenSans-Regular',
    fontSize: 24,
  },
});

type Props = {
  clipboard?: boolean,
  label: string,
  value: string,
};

export default class TextBoxField extends Component {
  props: Props;

  static defaultProps = {
    clipboard: false,
  };

  handleClipboard() {
    const { value } = this.props;
    Clipboard.setString(value);
    Toast.show('VA number is copied to clipboard');
  }

  render() {
    const { label, value, clipboard } = this.props;
    return (
      <View style={styles.container}>
        <Text style={styles.label}>{label}</Text>
        <View style={styles.box}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.value}>{value}</Text>
            {clipboard ? (
              <TouchableOpacity onPress={() => this.handleClipboard()}>
                <Icon name="content-copy" color={colors.white} size={30} />
              </TouchableOpacity>
            ) : null}
          </View>
        </View>
      </View>
    );
  }
}
