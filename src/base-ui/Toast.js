// @flow
import { Platform } from 'react-native';
import RNSimpleToast from 'react-native-simple-toast';

const Toast = {
  show: (message: string) => {
    if (Platform.OS === 'ios') {
      setTimeout(() => RNSimpleToast.show(message), 250);
    } else {
      RNSimpleToast.show(message);
    }
  },
};

export default Toast;
