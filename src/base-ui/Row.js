// @flow
import React from 'react';
import { View } from 'react-native';

import styles from './styles/Row-styles';

type Props = {
  style?: Style,
};

export default function Row(props: Props) {
  const { style, ...others } = props;
  return <View style={[styles.container, style]} {...others} />;
}

Row.defaultProps = {
  style: null,
};
