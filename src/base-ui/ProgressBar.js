// @flow
import React from 'react';
import { View } from 'react-native';

import styles from './styles/ProgressBar-styles';

type Props = {
  percentage: number,
};

export default function ProgressBar(props: Props) {
  const { percentage } = props;

  return (
    <View style={styles.container}>
      <View style={[styles.value, { flex: percentage / 100 }]} />
    </View>
  );
}
