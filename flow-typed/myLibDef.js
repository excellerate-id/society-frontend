// copied from StyleObj in react-native/Libraries/StyleSheet/StyleSheetTypes
type Atom = number | bool | Object | Array<?Atom>;
declare type Style = Atom;
