import React from 'react';
import * as mockRouterFlux from 'react-native-router-flux';

import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import AccountPage from '../../src/pages/AccountPage';
import ConfirmPasswordPage from '../../src/pages/ConfirmPasswordPage';
import EnterAccessCodePage from '../../src/pages/EnterAccessCodePage';
import EnterPasswordPage from '../../src/pages/EnterPasswordPage';
import CallUsPage from '../../src/pages/CallUsPage';
import ChooseBankPage from '../../src/pages/ChooseBankPage';
import ForgotPasswordPage from '../../src/pages/ForgotPasswordPage';
import InvestmentsPage from '../../src/pages/InvestmentsPage';
import LoginPage from '../../src/pages/LoginPage';
import OffersPage from '../../src/pages/OffersPage';
import ProductPage from '../../src/pages/ProductPage';
import RewardsPage from '../../src/pages/RewardsPage';
import ScheduleCallPage from '../../src/pages/ScheduleCallPage';
import SplashPage from '../../src/pages/SplashPage';
import SuccessPage from '../../src/pages/SuccessPage';
import TransactionsHistoryPage from '../../src/pages/TransactionsHistoryPage';
import VerifyDetailsPage from '../../src/pages/VerifyDetailsPage';
import VerifyMobileNumberPage from '../../src/pages/VerifyMobileNumberPage';
import VirtualAccountDetailsPage from '../../src/pages/VirtualAccountDetailsPage';
import WriteCheckPage from '../../src/pages/WriteCheckPage';

mockRouterFlux.Actions = {
  pop: action('pop navigation'),
  reset: action('reset navigation'),
  accessCodePage: action('navigate AccessCodePage'),
  accountPage: action('navigate AccountPage'),
  confirmPasswordPage: action('navigate ConfirmPasswordPage'),
  enterPasswordPage: action('navigate EnterPasswordPage'),
  forgotPasswordPage: action('navigate ForgotPasswordPage'),
  investementsPage: action('navigate InvestmentsPage'),
  loginPage: action('navigate LoginPage'),
  mainTabs: action('navigate MainTabs'),
  offersPage: action('navigate OffersPage'),
  productPage: action('navigate ProductPage'),
  rewardsPage: action('navigate RewardsPage'),
  scheduleCallPage: action('navigate ScheduleCallPage'),
  splashPage: action('navigate SplashPage'),
  successPage: action('navigate SuccessPage'),
  transactionsHistoryPage: action('navigate TransactionHistoryPage'),
  verifyDetailsPage: action('navigate VerifyDetailsPage'),
  verifyMobileNumberPage: action('navigate VerifyMobileNumberPage'),
  virtualAccountDetailsPage: action('navigate VirtualAccountDetailsPage'),
};

storiesOf('pages/AccountPage', module)
  .add('default', () => (
    <AccountPage />
  ));

storiesOf('pages/ConfirmPasswordPage', module)
  .add('default', () => (
    <ConfirmPasswordPage />
  ));

storiesOf('pages/CallUsPage', module)
  .add('default', () => (
    <CallUsPage />
  ));

storiesOf('pages/ChooseBankPage', module)
  .add('default', () => (
    <ChooseBankPage />
  ));

storiesOf('pages/EnterAccessCodePage', module)
  .add('default', () => (
    <EnterAccessCodePage />
  ));

storiesOf('pages/EnterPasswordPage', module)
  .add('default', () => (
    <EnterPasswordPage />
  ));

storiesOf('pages/ForgotPasswordPage', module)
  .add('default', () => (
    <ForgotPasswordPage />
  ));

storiesOf('pages/InvestmentsPage', module)
  .add('default', () => (
    <InvestmentsPage />
  ));

storiesOf('pages/LoginPage', module)
  .add('default', () => (
    <LoginPage />
  ));

storiesOf('pages/OffersPage', module)
  .add('default', () => (
    <OffersPage />
  ));

storiesOf('pages/ProductPage', module)
  .add('before invested', () => (
    <ProductPage />
  ))
  .add('after invested', () => (
    <ProductPage invested />
  ));

storiesOf('pages/RewardsPage', module)
  .add('default', () => (
    <RewardsPage />
  ));

storiesOf('pages/ScheduleCallPage', module)
  .add('default', () => (
    <ScheduleCallPage />
  ));

storiesOf('pages/SplashPage', module)
  .add('default', () => (
    <SplashPage label="Go to home" onLogin={linkTo('pages/SplashPage')} />
  ));

storiesOf('pages/SuccessPage', module)
  .add('create account', () => (
    <SuccessPage
      title="Congratulations!"
      description="You have successfully activated your access to Society 51 account. You now have access to exclusive investment opportunities right in the palm of your hand."
      label="Start Investing!"
      onConfirm={action('button clicked create account')}
    />
  ))
  .add('email sent', () => (
    <SuccessPage
      title="E-mail Sent!"
      description="Please check your e-mail for next instruction on resetting your password."
      label="Start Investing!"
      onConfirm={action('button clicked email sent')}
    />
  ))
  .add('call request', () => (
    <SuccessPage
      title="Thank You!"
      description="Our Relationship Manager has been notified and will be in touch with you soon."
      label="OK"
      onConfirm={action('button clicked call request')}
    />
  ));

storiesOf('pages/TransactionsHistoryPage', module)
  .add('default', () => (
    <TransactionsHistoryPage />
  ));

storiesOf('pages/VerifyDetailsPage', module)
  .add('default', () => (
    <VerifyDetailsPage />
  ));

storiesOf('pages/VerifyMobileNumberPage', module)
  .add('default', () => (
    <VerifyMobileNumberPage />
  ));

storiesOf('pages/VirtualAccountDetailsPage', module)
  .add('default', () => (
    <VirtualAccountDetailsPage />
  ));

storiesOf('pages/WriteCheckPage', module)
  .add('default', () => (
    <WriteCheckPage />
  ));
