import React, { Component } from 'react';

import DigitInput from '../../../src/base-ui/DigitInput';

export default class DigitInputDecorator extends Component {
  state = {
    digits: [-1, -1, -1, -1],
  };

  render() {
    return (
      <DigitInput
        digits={this.state.digits}
        onChangeDigits={digits => this.setState({ digits })}
      />
    );
  }
}
