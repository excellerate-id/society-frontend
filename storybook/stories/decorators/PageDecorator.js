// @flow
import React from 'react';
import { StyleSheet } from 'react-native';

import Page from '../../../src/base-ui/Page';

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
});

export default function PageDecorator(props: any) {
  const { style, ...others } = props;
  return <Page style={[styles.container, style]} {...others} />;
}
