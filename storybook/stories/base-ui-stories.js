import React from 'react';

import { storiesOf } from '@storybook/react-native';
import { action } from '@storybook/addon-actions';

import AccessCodeInput from '../../src/base-ui/AccessCodeInput';
import BulletNumber from '../../src/base-ui/BulletNumber';
import DigitInputDecorator from './decorators/DigitInputDecorator';
import FooterButton from '../../src/base-ui/FooterButton';
import Header from '../../src/base-ui/Header';
import InvestmentCard from '../../src/base-ui/InvestmentCard';
import InvestmentCardHeader from '../../src/base-ui/InvestmentCardHeader';
import Link from '../../src/base-ui/Link';
import MenuList from '../../src/base-ui/MenuList';
import OfferCard from '../../src/base-ui/OfferCard';
import PrimaryButton from '../../src/base-ui/PrimaryButton';
import ProgressBar from '../../src/base-ui/ProgressBar';
import Page from '../../src/base-ui/Page';
import PageBackground from '../../src/base-ui/PageBackground';
import RewardCard from '../../src/base-ui/RewardCard';
import SecondaryButton from '../../src/base-ui/SecondaryButton';
import Separator from '../../src/base-ui/Separator';
import StyledText from '../../src/base-ui/StyledText';
import TabIcon from '../../src/base-ui/TabIcon';
import TextInputField from '../../src/base-ui/TextInputField';
import TransactionHistoryCard from '../../src/base-ui/TransactionHistoryCard';

import PageDecorator from './decorators/PageDecorator';

import calendarImage from '../../src/images/ic_calendar.png';

storiesOf('base-ui/AccessCodeInput', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <AccessCodeInput />
  ));

storiesOf('base-ui/BulletNumber', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <BulletNumber label="2" />
  ));

storiesOf('base-ui/DigitInput', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <DigitInputDecorator />
  ));

storiesOf('base-ui/FooterButton', module)
  .addDecorator(getStory => <Page>{getStory()}</Page>)
  .add('default', () => (
    <FooterButton />
  ));

storiesOf('base-ui/Header', module)
  .addDecorator(getStory => <PageDecorator style={{ padding: 0 }}>{getStory()}</PageDecorator>)
  .add('default', () => (
    <Header onBack={action('back button clicked')} />
  ));

storiesOf('base-ui/InvestmentCard', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <InvestmentCard />
  ));

storiesOf('base-ui/InvestmentCardHeader', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <InvestmentCardHeader />
  ));

storiesOf('base-ui/Link', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <Link label="Link" onPress={action('link clicked')} />
  ));

storiesOf('base-ui/MenuList', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <MenuList
      items={[
        { imageSource: calendarImage, label: 'Schedule a Call' },
      ]}
    />
  ));

storiesOf('base-ui/OfferCard', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <OfferCard />
  ));

storiesOf('base-ui/PageBackground', module)
  .addDecorator(getStory => <Page>{getStory()}</Page>)
  .add('default', () => (
    <PageBackground />
  ));

storiesOf('base-ui/PrimaryButton', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <PrimaryButton label="Primary" onPress={action('primary button clicked')} />
  ));

storiesOf('base-ui/ProgressBar', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <ProgressBar percentage={0.8} />
  ));

storiesOf('base-ui/RewardCard', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <RewardCard />
  ));

storiesOf('base-ui/SecondaryButton', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <SecondaryButton label="Secondary" onPress={action('secondary button clicked')} />
  ));

storiesOf('base-ui/Separator', module)
  .addDecorator(getStory => <PageDecorator>{getStory()}</PageDecorator>)
  .add('default', () => (
    <Separator />
  ));

storiesOf('base-ui/StyledText', module)
  .add('default', () => (
    <PageDecorator>
      <StyledText font="Domine-Regular-28-40">h1Display</StyledText>
      <StyledText font="OpenSans-Regular-24-30">h2DisplayInput</StyledText>
      <StyledText font="OpenSans-SemiBold-18-26">h3Headline</StyledText>
      <StyledText font="OpenSans-SemiBoldItalic-18-26">button-primary</StyledText>
      <StyledText font="OpenSans-SemiBold-16-24">h4Title</StyledText>
      <StyledText font="OpenSans-SemiBold-16-24-underline">h4Link</StyledText>
      <StyledText font="OpenSans-Bold-14-20">h5Subtitle</StyledText>
      <StyledText font="OpenSans-SemiBold-14-20">paragraph</StyledText>
      <StyledText font="OpenSans-SemiBold-14-20-underline">paragraphLink</StyledText>
      <StyledText font="OpenSans-SemiBold-18">button-primary</StyledText>
      <StyledText font="OpenSans-SemiBold-14">button-secondary</StyledText>
      <StyledText font="OpenSans-Bold-11">h6Caption</StyledText>
      <StyledText font="OpenSans-Bold-16">ReturnDetails</StyledText>
      <StyledText font="OpenSans-SemiBold-28">Return</StyledText>
    </PageDecorator>
  ));

storiesOf('base-ui/TabIcon', module)
  .add('default', () => (
    <PageDecorator>
      <TabIcon name="offers" style={{ width: 64, height: 64 }} />
    </PageDecorator>
  ));

storiesOf('base-ui/TextInputField', module)
  .add('default', () => (
    <PageDecorator>
      <TextInputField label="LABEL" />
    </PageDecorator>
  ));

storiesOf('base-ui/TransactionHistoryCard', module)
  .add('default', () => (
    <PageDecorator>
      <TransactionHistoryCard />
    </PageDecorator>
  ));
